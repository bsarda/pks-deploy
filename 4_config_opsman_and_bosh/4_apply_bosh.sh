#!/bin/bash

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    touch "${VAR_FILE_INFO}"
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# get the token
uaac target https://${OPSMAN_HOST}/uaa --skip-ssl-validation
uaac token owner get opsman ${OPSMAN_ADMIN} -p ${OPSMAN_PASSWORD} -s ""
uaac_token=$(uaac context | grep access_token | sed 's/[[:space:]]*access_token: //')

echo "Proceeding on BOSH Director apply"
curl -sk "https://${OPSMAN_HOST}/api/v0/installations" -X POST -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d '{"deploy_products": "none","ignore_warnings": true}'

# wait until it's fully applied
while [[ "$(curl -sk "https://${OPSMAN_HOST}/api/v0/installations" -H "Authorization: Bearer ${uaac_token}")" == *"running"* ]]
do
  echo "Installation is still runnning..."
  sleep 10
done
echo "BOSH deployment finished"

# get version, guid and other
bosh_deployed_name_ver_guid=$(curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products" -H "Authorization: Bearer ${uaac_token}" | python3 -c '
import sys, json;
json_object=json.load(sys.stdin);
print(json_object[0]["installation_name"] +"/"+ json_object[0]["product_version"] +"/"+ json_object[0]["guid"])')
bosh_deployed_name=$(echo ${bosh_deployed_name_ver_guid} | awk -F"/" '{print $1}')
bosh_deployed_ver=$(echo ${bosh_deployed_name_ver_guid} | awk -F"/" '{print $2}')
bosh_deployed_guid=$(echo ${bosh_deployed_name_ver_guid} | awk -F"/" '{print $3}')

# get AZ guid-s
az_guids=$(curl -sk "https://${OPSMAN_HOST}/api/v0/staged/director/availability_zones" -H "Authorization: Bearer ${uaac_token}" | python3 -c '
import sys, json;
json_object=json.load(sys.stdin);
for json_sub in json_object["availability_zones"]:
  print(json_sub["name"] +"/"+ json_sub["guid"])')
for az_guid in $az_guids
do
  echo "bosh_az_guid_$(echo ${az_guid} | awk -F"/" '{print $1}')=$(echo ${az_guid} | awk -F"/" '{print $2}')" >> ${VAR_FILE_INFO}
done

# store in file
echo "bosh_name=${bosh_deployed_name}" >> ${VAR_FILE_INFO}
echo "bosh_ver=${bosh_deployed_ver}" >> ${VAR_FILE_INFO}
echo "bosh_guid=${bosh_deployed_guid}" >> ${VAR_FILE_INFO}

# remove token
unset uaac_token
# clear
unset bosh_deployed_name_ver_guid
unset bosh_deployed_name
unset bosh_deployed_ver
unset bosh_deployed_guid
unset az_guids

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
