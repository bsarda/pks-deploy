#!/bin/bash

# import the global variables
. ../0_env.sh

body="{ \
    \"setup\": { \
    \"decryption_passphrase\": \"${OPSMAN_PASSPHRASE}\", \
    \"decryption_passphrase_confirmation\":\"${OPSMAN_PASSWORD}\", \
    \"eula_accepted\": \"true\", \
    \"identity_provider\": \"internal\", \
    \"admin_user_name\": \"${OPSMAN_ADMIN}\", \
    \"admin_password\": \"${OPSMAN_PASSWORD}\", \
    \"admin_password_confirmation\": \"${OPSMAN_PASSWORD}\", \
    \"http_proxy\": \"\", \
    \"https_proxy\": \"\", \
    \"no_proxy\": \"\" \
  } }"
# initializing with local auth, single user for now
echo "initializing with local auth, single user '${OPSMAN_ADMIN}' with given password and passphrase"
curl -sk -X POST -H "Content-Type: application/json" -d "${body}" "https://${OPSMAN_HOST}/api/v0/setup"
unset body
# you have to wait 5min until available
echo -e "\nwill wait for 1 min..."
sleep 60
# while different from 200, loop
opsman_login_rc=0
while [ $opsman_login_rc != 200 ]
do
   opsman_login_rc=$(curl -Lks -o /dev/null -w "%{http_code}" "https://${OPSMAN_HOST}/login/ensure_availability")
   sleep 10
done
unset opsman_login_rc

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
