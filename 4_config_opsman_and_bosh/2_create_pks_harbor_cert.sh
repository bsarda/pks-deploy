#!/bin/bash

# import the global variables
. ../0_env.sh

# create config file
cat << EOF > pks-cert.cnf
[ req ]
default_bits = 2048
distinguished_name = req_distinguished_name
req_extensions = req_ext
prompt = no
[ req_distinguished_name ]
countryName = ${PKS_CERT_COUNTRY}
stateOrProvinceName = ${PKS_CERT_STATE}
localityName = ${PKS_CERT_LOCALITY}
organizationName = ${PKS_CERT_ORG}
commonName = ${PKS_HOST}
[ req_ext ]
subjectAltName=DNS:${PKS_HOST}
EOF
echo "create PKS API certificate"
openssl req -newkey rsa:2048 -x509 -nodes -keyout ${PKS_CERT_KEY} -new -out ${PKS_CERT_FILE} -extensions req_ext -config pks-cert.cnf -sha256 -days 3650
rm pks-cert.cnf

cat << EOF > harbor-cert.cnf
[ req ]
default_bits = 2048
distinguished_name = req_distinguished_name
req_extensions = req_ext
prompt = no
[ req_distinguished_name ]
countryName = ${HARBOR_CERT_COUNTRY}
stateOrProvinceName = ${HARBOR_CERT_STATE}
localityName = ${HARBOR_CERT_LOCALITY}
organizationName = ${HARBOR_CERT_ORG}
commonName = ${HARBOR_HOST}
[ req_ext ]
subjectAltName=DNS:${HARBOR_HOST}
EOF
echo "create PKS API certificate"
openssl req -newkey rsa:2048 -x509 -nodes -keyout ${HARBOR_CERT_KEY} -new -out ${HARBOR_CERT_FILE} -extensions req_ext -config harbor-cert.cnf -sha256 -days 3650
rm harbor-cert.cnf

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
