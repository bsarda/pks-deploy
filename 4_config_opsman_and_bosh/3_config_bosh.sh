#!/bin/bash

# import the global variables
. ../0_env.sh

# get the token
uaac target https://${OPSMAN_HOST}/uaa --skip-ssl-validation
uaac token owner get opsman ${OPSMAN_ADMIN} -p ${OPSMAN_PASSWORD} -s ""
uaac_token=$(uaac context | grep access_token | sed 's/[[:space:]]*access_token: //')

echo "Proceeding on BOSH Director config for : IaaS, Security, DNS, Logging"
# sending the configuration of iaas, security, logging and dns
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/director/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"iaas_configuration\": { \
            \"name\": \"${BOSH_IAAS_NAME}\", \
            \"vcenter_host\": \"${BOSH_IAAS_HOST}\", \
            \"datacenter\": \"${BOSH_IAAS_DATACENTER}\", \
            \"ephemeral_datastores_string\": \"${BOSH_IAAS_DATASTORE_EPHEMERAL}\", \
            \"persistent_datastores_string\": \"${BOSH_IAAS_DATASTORE_PERSISTANT}\", \
            \"vcenter_username\": \"${BOSH_IAAS_ADMIN}\", \
            \"vcenter_password\": \"${BOSH_IAAS_PASSWORD}\", \
            \"nsx_networking_enabled\": true, \
            \"nsx_mode\": \"nsx-t\", \
            \"nsx_address\": \"${NSX_HOST}\", \
            \"nsx_username\": \"${BOSH_NSX_ADMIN}\", \
            \"nsx_password\": \"${BOSH_NSX_PASSWORD}\", \
            \"nsx_ca_certificate\": \"$(cat ${NSX_CERT_FILE} | sed ':a;N;$!ba;s/\n/\\r\\n/g')\", \
            \"bosh_vm_folder\": \"${BOSH_IAAS_FOLDER_VM}\", \
            \"bosh_template_folder\": \"${BOSH_IAAS_FOLDER_TEMPLATES}\", \
            \"bosh_disk_path\": \"${BOSH_IAAS_FOLDER_DISK}\", \
            \"disk_type\": \"thin\", \
            \"ssl_verification_enabled\": false \
          } \
        }"

curl -sk "https://${OPSMAN_HOST}/api/v0/staged/director/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"director_configuration\": { \
            \"ntp_servers_string\": \"${NTPSERVER}\", \
            \"metrics_ip\": null, \
            \"resurrector_enabled\": true, \
            \"allow_legacy_agents\": true, \
            \"director_hostname\": \"\", \
            \"max_threads\": 5, \
            \"director_worker_count\": 5, \
            \"post_deploy_enabled\": true, \
            \"bosh_recreate_on_next_deploy\": false, \
            \"bosh_recreate_persistent_disks_on_next_deploy\": false, \
            \"retry_bosh_deploys\": false, \
            \"keep_unreachable_vms\": false, \
            \"skip_director_drain\": true, \
            \"database_type\": \"internal\", \
            \"blobstore_type\": \"local\", \
            \"local_blobstore_options\": {\"tls_enabled\": true}, \
            \"hm_pager_duty_options\": {\"enabled\": false}, \
            \"hm_emailer_options\": {\"enabled\": false} \
          } \
        }"

curl -sk "https://${OPSMAN_HOST}/api/v0/staged/director/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"dns_configuration\": { \
            \"recursor_timeout\": \"4s\", \
            \"handlers\": [ \
              { \
                \"cache\": { \
                  \"enabled\": true \
                }, \
                \"domain\": \"corp.local\", \
                \"source\": { \
                  \"type\": \"dns\", \
                  \"recursors\": [\"${DNSSERVER}\"] \
                } \
              } \
            ] \
          }, \
          \"security_configuration\": { \
            \"trusted_certificates\": \"$(cat ${HARBOR_CERT_FILE} | sed ':a;N;$!ba;s/\n/\\r\\n/g')\", \
            \"opsmanager_root_ca_trusted_certs\": true, \
            \"generate_vm_passwords\": true \
          }, \
          \"syslog_configuration\": { \
            \"enabled\": true, \
            \"address\": \"${SYSLOGSERVER}\", \
            \"port\": \"514\", \
            \"transport_protocol\": \"udp\", \
            \"tls_enabled\": false \
          } \
        }"

# wait a bit before autoconf
sleep 5
# get iaas config guid
#bosh_iaas_configuration_guid=$(curl -sk "https://${OPSMAN_HOST}/api/v0/staged/director/properties" -H "Authorization: Bearer ${uaac_token}" | python3 -c "import sys, json; print(json.load(sys.stdin)['iaas_configuration']['guid'])")
#        \"iaas_configuration_guid\": \"${bosh_iaas_configuration_guid}\", \
echo "Proceeding on BOSH Director config for : Availability zones"
# support for 9 az in this script
for i in {1..9} ; do
  varname="BOSH_CONFIG_AZ_${i}_NAME"
  if [ -z ${!varname+x} ]; then break; fi
  varrp="BOSH_CONFIG_AZ_${i}_CLUSTER_RESOURCEPOOL"
  echo "    Creating the AZ '${!varname}', located on '${!varrp}'"
  curl -sk "https://${OPSMAN_HOST}/api/v0/staged/director/availability_zones" -X POST -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
    \"availability_zone\": { \
        \"name\": \"${!varname}\", \
        \"clusters\": [ \
          { \
            \"cluster\": \"$(echo ${!varrp} | sed 's@/.*@@')\", \
            \"resource_pool\": \"$(echo ${!varrp} | sed 's@.*/@@')\" \
          } \
        ] \
      } \
    }"
done
# and the infra az
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/director/availability_zones" -X POST -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
    \"availability_zone\": { \
        \"name\": \"${BOSH_CONFIG_AZ_I_NAME}\", \
        \"clusters\": [ \
          { \
            \"cluster\": \"$(echo ${BOSH_CONFIG_AZ_I_CLUSTER_RESOURCEPOOL} | sed 's@/.*@@')\", \
            \"resource_pool\": \"$(echo ${BOSH_CONFIG_AZ_I_CLUSTER_RESOURCEPOOL} | sed 's@.*/@@')\" \
          } \
        ] \
      } \
    }"

# post the networks creation (all the conf is in the env)
echo "Proceeding on BOSH Director config for : Networks"
# OLD, with service network:
# curl -sk "https://${OPSMAN_HOST}/api/v0/staged/director/networks" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \"icmp_checks_enabled\": false, \"networks\": [ ${BOSH_NETWORKS_I},${BOSH_NETWORKS_AZ} ] }"
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/director/networks" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \"icmp_checks_enabled\": true, \"networks\": [ ${BOSH_NETWORKS} ] }"

# post the network<>az relationship
echo "Proceeding on BOSH Director config for : Networks to AZ map for singleton jobs"
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/director/network_and_az" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
  \"network_and_az\": { \
      \"network\": { \
        \"name\": \"$(echo ${BOSH_NET2AZ} | sed 's@/.*@@')\" \
      }, \
      \"singleton_availability_zone\": { \
        \"name\": \"$(echo ${BOSH_NET2AZ} | sed 's@.*/@@')\" \
      } \
  } \
}"

# remove token
unset uaac_token

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
