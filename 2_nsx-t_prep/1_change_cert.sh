#!/bin/bash

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    touch "${VAR_FILE_INFO}"
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# create config file
cat << EOF > nsx-cert.cnf
[ req ]
default_bits = 2048
distinguished_name = req_distinguished_name
req_extensions = req_ext
prompt = no
[ req_distinguished_name ]
countryName = ${NSX_CERT_COUNTRY}
stateOrProvinceName = ${NSX_CERT_STATE}
localityName = ${NSX_CERT_LOCALITY}
organizationName = ${NSX_CERT_ORG}
commonName = ${NSX_HOST}
[ req_ext ]
subjectAltName=DNS:${NSX_HOST}
EOF
echo "create NSX ui/api certificate"
openssl req -newkey rsa:2048 -x509 -nodes -keyout ${NSX_CERT_KEY} -new -out ${NSX_CERT_FILE} -reqexts req_ext -extensions req_ext -config nsx-cert.cnf -sha256 -days 3650
rm nsx-cert.cnf

echo "add cert in NSX truststore"
cert_api=$(cat <<END
  {
    "display_name": "${NSX_HOST}",
    "pem_encoded": "$(cat ${NSX_CERT_FILE} |  sed ':a;N;$!ba;s/\n/\\n/g')",
    "private_key": "$(cat ${NSX_CERT_KEY} |  sed ':a;N;$!ba;s/\n/\\n/g')"
  }
END
)
importcert=$(curl -k -X POST "https://${NSX_HOST}/api/v1/trust-management/certificates?action=import" -u "${NSX_ADMIN}:${NSX_PASSWORD}" -H 'content-type: application/json' -d "${cert_api}")
unset cert_api
nsx_host_certid=$(echo $importcert | python3 -c "import sys, json; print(json.load(sys.stdin)['results'][0]['id'])")
unset importcert
# adding to the file
echo "nsx_host_certid=${nsx_host_certid}" >> ${VAR_FILE_INFO}
echo "Change NSX cert of UI/API"
curl --insecure -u "${NSX_ADMIN}:${NSX_PASSWORD}" -X POST "https://${NSX_HOST}/api/v1/node/services/http?action=apply_certificate&certificate_id=${nsx_host_certid}"
unset nsx_host_certid

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
