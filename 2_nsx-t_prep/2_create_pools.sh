#!/bin/bash

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    touch "${VAR_FILE_INFO}"
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# create node ip block
echo "Creating Nodes IP block"
nodeipblock="{ \
    \"display_name\": \"${NODE_IP_BLOCK_NAME}\", \
    \"description\": \"${NODE_IP_BLOCK_DESC}\", \
    \"cidr\": \"${NODE_IP_BLOCK_CIDR}\"
  }"
nodes_ip_block_id=$(curl -sS -H "Content-Type: application/json" -k -u ${NSX_ADMIN}:${NSX_PASSWORD} -X POST -d "${nodeipblock}" https://${NSX_HOST}/api/v1/pools/ip-blocks | python3 -c "import sys, json; print(json.load(sys.stdin)['id'])")
# adding to the file
echo "nodes_ip_block_id=${nodes_ip_block_id}" >> ${VAR_FILE_INFO}
unset nodeipblock
unset nodes_ip_block_id

# create pod ip block
echo "Creating Pods IP block"
podipblock="{ \
    \"display_name\": \"${POD_IP_BLOCK_NAME}\", \
    \"description\": \"${POD_IP_BLOCK_DESC}\", \
    \"cidr\": \"${POD_IP_BLOCK_CIDR}\"
  }"
pods_ip_block_id=$(curl -sS -H "Content-Type: application/json" -k -u ${NSX_ADMIN}:${NSX_PASSWORD} -X POST -d "${podipblock}" https://${NSX_HOST}/api/v1/pools/ip-blocks | python3 -c "import sys, json; print(json.load(sys.stdin)['id'])")
# adding to the file
echo "pods_ip_block_id=${pods_ip_block_id}" >> ${VAR_FILE_INFO}
unset podipblock
unset pods_ip_block_id

# create vip pool
echo "Creating VIP IP pool"
ippool="{ \
    \"display_name\": \"${VIP_IP_POOL_NAME}\", \
    \"description\": \"${VIP_IP_POOL_DESC}\", \
    \"subnets\": [ \
      { \
          \"dns_nameservers\": [], \
          \"allocation_ranges\": [ \
              { \
                  \"start\": \"${VIP_IP_POOL_ALLOCATION_START}\", \
                  \"end\": \"${VIP_IP_POOL_ALLOCATION_END}\"
              } \
          ], \
          \"cidr\": \"${VIP_IP_POOL_ALLOCATION_CIDR}\"
      }
    ]
  }"
vip_ip_pool_id=$(curl -sS -H "Content-Type: application/json" -k -u ${NSX_ADMIN}:${NSX_PASSWORD} -X POST -d "${ippool}" https://${NSX_HOST}/api/v1/pools/ip-pools | python3 -c "import sys, json; print(json.load(sys.stdin)['id'])")
# adding to the file
echo "vip_ip_pool_id=${vip_ip_pool_id}" >> ${VAR_FILE_INFO}
unset ippool
unset vip_ip_pool_id

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
