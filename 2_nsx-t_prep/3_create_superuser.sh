#!/bin/bash

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    touch "${VAR_FILE_INFO}"
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# create
openssl req -newkey rsa:2048 -x509 -nodes -keyout "${NSX_SUPERUSER_KEY_FILE}" -new -out "${NSX_SUPERUSER_CERT_FILE}" -subj /CN=${NSX_SUPERUSER_NAME} -extensions client_server_ssl -config <(
cat /etc/ssl/openssl.cnf \
<(printf '[client_server_ssl]\nextendedKeyUsage = clientAuth\n')
) -sha256 -days 3650
cert_request=$(cat <<END
  {
    "display_name": "${NSX_SUPERUSER_NAME}",
    "pem_encoded": "$(awk '{printf "%s\\n", $0}' ${NSX_SUPERUSER_CERT_FILE})"
  }
END
)
importcert=$(curl -k -X POST "https://${NSX_HOST}/api/v1/trust-management/certificates?action=import" -u "${NSX_ADMIN}:${NSX_PASSWORD}" -H 'content-type: application/json' -d "${cert_request}")
unset cert_request
certificate_id=$(echo $importcert | python3 -c "import sys, json; print(json.load(sys.stdin)['results'][0]['id'])")
unset importcert
pi_request=$(cat <<END
  {
    "display_name": "${NSX_SUPERUSER_NAME}",
    "name": "${NSX_SUPERUSER_NAME}",
    "permission_group": "superusers",
    "certificate_id": "${certificate_id}",
    "node_id": "${NODE_ID}"
  }
END
)
curl -sk -X POST "https://${NSX_HOST}/api/v1/trust-management/principal-identities" -u "${NSX_ADMIN}:${NSX_PASSWORD}" -H 'content-type: application/json' -d "${pi_request}"
unset pi_request
# for verification only:
# curl -k -X GET "https://${NSX_HOST}/api/v1/trust-management/principal-identities" --cert ${NSX_SUPERUSER_CERT_FILE} --key ${NSX_SUPERUSER_KEY_FILE}

# adding to the file
echo "superuser_node_id=${NODE_ID}" >> ${VAR_FILE_INFO}
echo "superuser_certificate_id=${certificate_id}" >> ${VAR_FILE_INFO}
unset certificate_id

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
