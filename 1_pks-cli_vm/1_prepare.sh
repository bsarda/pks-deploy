#!/bin/bash

# import the global variables
. ../0_env.sh

# start
sudo apt update && sudo apt upgrade -y
sudo apt install -y wget unzip curl openssh-server python3 git nfs-common traceroute gawk bash-completion

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
