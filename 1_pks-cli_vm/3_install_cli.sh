#!/bin/bash

# import the global variables
. ../0_env.sh

# cleanup util
curl -LOs https://storage.googleapis.com/pks-releases/pks_cleanup_linux
chmod +x pks_cleanup_linux
sudo mv -f pks_cleanup_linux ${BINDIR}/pks_cleanup
# uaac
sudo apt -y install ruby ruby-dev gcc build-essential g++
sudo gem install cf-uaac
uaac -v
# om
OM_VER=$(curl -ks https://github.com/pivotal-cf/om/releases/latest | sed 's@.*tag/@@' | sed 's@">.*@@')
wget https://github.com/pivotal-cf/om/releases/download/${OM_VER}/om-linux
chmod +x om-linux
sudo mv -f om-linux ${BINDIR}/om
unset OM_VER
# bosh-cli
BOSH_VER=$(curl -ks https://github.com/cloudfoundry/bosh-cli/releases/latest | sed 's@.*tag/v@@' | sed 's@">.*@@')
wget https://github.com/cloudfoundry/bosh-cli/releases/download/v${BOSH_VER}/bosh-cli-${BOSH_VER}-linux-amd64
chmod +x bosh-cli-${BOSH_VER}-linux-amd64
sudo mv -f bosh-cli-${BOSH_VER}-linux-amd64 ${BINDIR}/bosh
unset BOSH_VER
bosh -v
# nsx-cli
sudo apt -y install git httpie
wget https://storage.googleapis.com/pks-releases/nsx-helper-pkg.tar.gz
tar -xvzf nsx-helper-pkg.tar.gz -C /usr/local/bin
rm nsx-helper-pkg.tar.gz
# helm
HELM_VER=$(curl -ks https://github.com/helm/helm/releases/latest | sed 's@.*tag/@@' | sed 's@">.*@@')
wget https://storage.googleapis.com/kubernetes-helm/helm-${HELM_VER}-linux-amd64.tar.gz
tar xvzf helm-${HELM_VER}-linux-amd64.tar.gz linux-amd64/helm
chmod +x linux-amd64/helm
sudo mv -f linux-amd64/helm ${BINDIR}/helm
rm -fr linux-amd64
rm helm-${HELM_VER}-linux-amd64.tar.gz
unset HELM_VER
# openfaas-cli
curl -sSL https://cli.openfaas.com | sudo sh
faas-cli version
# docker (for ubuntu)
sudo apt install -y apt-transport-https ca-certificates software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && sudo apt update
sudo apt install -y docker-ce
# golang
GO_VER=$(wget -qO- https://golang.org/dl/ | grep -E 'downloadBox.*go[0-9\.]+.linux-amd64.tar.gz' | sed 's@.*downloadBox.*go/go@@' | sed 's@\.linux.*@@')
GO_DLLINK=$(wget -qO- https://golang.org/dl/ | grep -E 'downloadBox.*go[0-9\.]+.linux-amd64.tar.gz' | sed 's@.*href="@@' | sed 's@">.*@@')
wget ${GO_DLLINK}
sudo tar -xvf go${GO_VER}.linux-amd64.tar.gz
rm go${GO_VER}.linux-amd64.tar.gz
sudo mv -f go ${GOROOT}
mkdir -p ${GOPATH}
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
go version
unset GO_VER
unset GO_DLLINK
# govc cli
GOVC_VER=$(curl -ks https://github.com/vmware/govmomi/releases/latest/ | sed 's@.*tag/@@' | sed 's@">.*@@')
wget https://github.com/vmware/govmomi/releases/download/${GOVC_VER}/govc_linux_amd64.gz
gzip -d govc_linux_amd64.gz
chmod +x govc_linux_amd64
sudo mv -f govc_linux_amd64 ${BINDIR}/govc
govc version
unset GOVC_VER
# dispatch-cli
DISPATCH_VER=$(curl -s https://api.github.com/repos/vmware/dispatch/releases/latest | python3 -c "import sys, json; print(json.load(sys.stdin)['name'])")
curl -OLs https://github.com/vmware/dispatch/releases/download/${DISPATCH_VER}/dispatch-linux
chmod +x dispatch-linux
sudo mv -f dispatch-linux ${BINDIR}/dispatch
dispatch version
unset DISPATCH_VER

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
