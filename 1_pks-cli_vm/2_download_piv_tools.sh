# Doc is here:
# https://community.pivotal.io/s/article/How-to-Download-and-Upload-Pivotal-Cloud-Foundry-Products-via-API
# https://network.pivotal.io/docs/api#how-to-authenticate%20for%20new%20UAA%20API%20Token%20mechanism

# import the global variables
. ../0_env.sh

# get access token
pivtoken=$(curl -sX POST https://network.pivotal.io/api/v2/authentication/access_tokens -d "{\"refresh_token\":\"${PIVNET_REFRESH_TOKEN}\"}" | python3 -c "import sys, json; print(json.load(sys.stdin)['access_token'])")

# get latest PKS releases
# url_pks_pf=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/pivotal-container-service/releases" | python3 -c "
# import sys, json, os;
# json_object=json.load(sys.stdin);
# for json_rel in json_object['releases']:
#   if json_rel['version'].startswith('${VERSIONS_DESIRED_PKS}'):
#     print( json_rel['_links']['product_files']['href']  )
#     sys.exit(0)")
url_pks_fg=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/pivotal-container-service/releases"| python3 -c "
import sys, json, os;
json_object=json.load(sys.stdin);
for json_rel in json_object['releases']:
  if json_rel['version'].startswith('${VERSIONS_DESIRED_PKS}'):
    print( json_rel['_links']['file_groups']['href']  )
    sys.exit(0)")
# unset url_pks_pf

# accept eula
url_pks_eula=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/pivotal-container-service/releases" | python3 -c "
import sys, json, os;
json_object=json.load(sys.stdin);
for json_rel in json_object['releases']:
  if json_rel['version'].startswith('${VERSIONS_DESIRED_PKS}'):
    print( json_rel['_links']['eula_acceptance']['href']  )
    sys.exit(0)")
curl -s -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer ${pivtoken}" -X POST ${url_pks_eula}

# cli downloads
cli_pks_name=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_pks_fg}" | python3 -c "
import sys, re, json;
json_object=json.load(sys.stdin)['file_groups'];
for cliutil in json_object:
  if 'pks' in cliutil['name'].lower():
    for cliver in cliutil['product_files']:
       if 'linux' in cliver['name'].lower():
         print(cliver['aws_object_key'])" | sed 's@.*/@@')
cli_pks_download=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_pks_fg}" | python3 -c "
import sys, re, json;
json_object=json.load(sys.stdin)['file_groups'];
for cliutil in json_object:
  if 'pks' in cliutil['name'].lower():
    for cliver in cliutil['product_files']:
       if 'linux' in cliver['name'].lower():
         print(cliver['_links']['download']['href'])")

cli_kube_name=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_pks_fg}" | python3 -c "
import sys, re, json;
json_object=json.load(sys.stdin)['file_groups'];
for cliutil in json_object:
  if 'kube' in cliutil['name'].lower():
    for cliver in cliutil['product_files']:
       if 'linux' in cliver['name'].lower():
         print(cliver['aws_object_key'])" | sed 's@.*/@@')
cli_kube_download=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_pks_fg}" | python3 -c "
import sys, re, json;
json_object=json.load(sys.stdin)['file_groups'];
for cliutil in json_object:
  if 'kube' in cliutil['name'].lower():
    for cliver in cliutil['product_files']:
       if 'linux' in cliver['name'].lower():
         print(cliver['_links']['download']['href'])")

unset url_pks_fg

# tooling download
wget -O ${cli_pks_name} --header="Authorization: Bearer ${pivtoken}" ${cli_pks_download}
unset cli_pks_name
unset cli_pks_download
wget -O ${cli_kube_name} --header="Authorization: Bearer ${pivtoken}" ${cli_kube_download}
unset cli_kube_name
unset cli_kube_download
# kubectl autocomplete
echo "source <(kubectl completion bash)" >> ~/.bashrc
source ~/.bashrc
# remove token
unset pivtoken
# work on piv tools
chmod +x pks-linux-*
chmod +x kubectl-linux-*
sudo mv -f pks-linux-* ${BINDIR}/pks
sudo mv -f kubectl-* ${BINDIR}/kubectl
pks --version
kubectl version

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
