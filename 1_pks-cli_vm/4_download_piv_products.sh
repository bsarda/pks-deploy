#!/bin/bash
# Doc is here:
# https://community.pivotal.io/s/article/How-to-Download-and-Upload-Pivotal-Cloud-Foundry-Products-via-API
# https://network.pivotal.io/docs/api#how-to-authenticate%20for%20new%20UAA%20API%20Token%20mechanism

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    touch "${VAR_FILE_INFO}"
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# get access token
pivtoken=$(curl -sX POST https://network.pivotal.io/api/v2/authentication/access_tokens -d "{\"refresh_token\":\"${PIVNET_REFRESH_TOKEN}\"}" | python3 -c "import sys, json; print(json.load(sys.stdin)['access_token'])")

# ------------------------------------------------------------------
# get latest Opsman releases
url_om_pf=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/ops-manager/releases" | python3 -c "
import sys, json, os;
json_object=json.load(sys.stdin);
for json_rel in json_object['releases']:
  if json_rel['version'].startswith('${VERSIONS_DESIRED_OPSMAN}'):
    print( json_rel['_links']['product_files']['href']  )
    sys.exit(0)")
# accept eula
url_om_eula=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/ops-manager/releases" | python3 -c "
import sys, json, os;
json_object=json.load(sys.stdin);
for json_rel in json_object['releases']:
  if json_rel['version'].startswith('${VERSIONS_DESIRED_OPSMAN}'):
    print( json_rel['_links']['eula_acceptance']['href']  )
    sys.exit(0)")
curl -s -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer ${pivtoken}" -X POST ${url_om_eula}
# download info
url_om_download=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_om_pf}" | python3 -c "import sys, json; dict = json.load(sys.stdin)['product_files']; print( [obj for obj in dict if(obj['file_type'] == 'Software')] [0]['_links']['download']['href'])")
url_om_product=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_om_pf}" | python3 -c "import sys, json; dict = json.load(sys.stdin)['product_files']; print( [obj for obj in dict if(obj['file_type'] == 'Software')] [0]['_links']['self']['href'])")
url_om_filename=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_om_product}" | python3 -c "import sys, json; print(json.load(sys.stdin)['product_file']['aws_object_key'])" | sed 's@.*/@@')
# unset
unset url_om_pf
unset url_om_eula
unset url_om_product

# ------------------------------------------------------------------
# get latest PKS releases
url_pks_pf=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/pivotal-container-service/releases" | python3 -c "
import sys, json, os;
json_object=json.load(sys.stdin);
for json_rel in json_object['releases']:
  if json_rel['version'].startswith('${VERSIONS_DESIRED_PKS}'):
    print( json_rel['_links']['product_files']['href']  )
    sys.exit(0)")
# accept eula
url_pks_eula=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/pivotal-container-service/releases" | python3 -c "
import sys, json, os;
json_object=json.load(sys.stdin);
for json_rel in json_object['releases']:
  if json_rel['version'].startswith('${VERSIONS_DESIRED_PKS}'):
    print( json_rel['_links']['eula_acceptance']['href']  )
    sys.exit(0)")
curl -s -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer ${pivtoken}" -X POST ${url_pks_eula}
# download info
url_pks_download=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_pks_pf}" | python3 -c "import sys, json; dict = json.load(sys.stdin)['product_files']; print( [obj for obj in dict if(obj['file_type'] == 'Software')] [0]['_links']['download']['href'])")
url_pks_product=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_pks_pf}" | python3 -c "import sys, json; dict = json.load(sys.stdin)['product_files']; print( [obj for obj in dict if(obj['file_type'] == 'Software')] [0]['_links']['self']['href'])")
url_pks_filename=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_pks_product}" | python3 -c "import sys, json; print(json.load(sys.stdin)['product_file']['aws_object_key'])" | sed 's@.*/@@')
# unset
unset url_pks_pf
unset url_pks_eula
unset url_pks_product

# ------------------------------------------------------------------
# get latest Harbor releases
url_harbor_pf=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/harbor-container-registry/releases" | python3 -c "
import sys, json, os;
json_object=json.load(sys.stdin);
for json_rel in json_object['releases']:
  if json_rel['version'].startswith('${VERSIONS_DESIRED_HARBOR}'):
    print( json_rel['_links']['product_files']['href']  )
    sys.exit(0)")
# accept eula
url_harbor_eula=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/harbor-container-registry/releases" | python3 -c "
import sys, json, os;
json_object=json.load(sys.stdin);
for json_rel in json_object['releases']:
  if json_rel['version'].startswith('${VERSIONS_DESIRED_HARBOR}'):
    print( json_rel['_links']['eula_acceptance']['href']  )
    sys.exit(0)")
curl -s -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer ${pivtoken}" -X POST ${url_harbor_eula}
# download info
url_harbor_download=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_harbor_pf}" | python3 -c "import sys, json; dict = json.load(sys.stdin)['product_files']; print( [obj for obj in dict if(obj['file_type'] == 'Software')] [0]['_links']['download']['href'])")
url_harbor_product=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_harbor_pf}" | python3 -c "import sys, json; dict = json.load(sys.stdin)['product_files']; print( [obj for obj in dict if(obj['file_type'] == 'Software')] [0]['_links']['self']['href'])")
url_harbor_filename=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_harbor_product}" | python3 -c "import sys, json; print(json.load(sys.stdin)['product_file']['aws_object_key'])" | sed 's@.*/@@')
# unset
unset url_harbor_pf
unset url_harbor_eula
unset url_harbor_product

# ------------------------------------------------------------------
# get latest PFS releases
# url_pfs_pf=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/pivotal-function-service/releases" | python3 -c "
# import sys, json, os;
# json_object=json.load(sys.stdin);
# for json_rel in json_object['releases']:
#   if json_rel['version'].startswith('${VERSIONS_DESIRED_PFS}'):
#     print( json_rel['_links']['product_files']['href']  )
#     sys.exit(0)
# print(json_object['releases'][0]['_links']['product_files']['href'])")
# # accept eula
# url_pfs_eula=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/pivotal-function-service/releases" | python3 -c "
# import sys, json, os;
# json_object=json.load(sys.stdin);
# for json_rel in json_object['releases']:
#   if json_rel['version'].startswith('${VERSIONS_DESIRED_PFS}'):
#     print( json_rel['_links']['eula_acceptance']['href']  )
#     sys.exit(0)")
# curl -s -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer ${pivtoken}" -X POST ${url_pfs_eula}
# # download
# url_pfs_download=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_pfs_pf}" | python3 -c "import sys, json; dict = json.load(sys.stdin)['product_files']; print( [obj for obj in dict if(obj['file_type'] == 'Software')] [0]['_links']['download']['href'])")
# url_pfs_filename=$(curl -sX GET $(echo $url_pfs_download | sed 's@/download@@') -H "Authorization: Bearer ${pivtoken}" | python3 -c "import sys, json; print(json.load(sys.stdin)['product_file']['included_files'][0])")
# # unset
# unset url_harbor_pf
# unset url_harbor_eula
# unset url_harbor_product

# ------------------------------------------------------------------
# get latest and n-1 stemcell
url_stemcell_latest_pf=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/stemcells-ubuntu-xenial/releases" | python3 -c "import sys, json; print(json.load(sys.stdin)['releases'][0]['_links']['product_files']['href'])")
# accept eula
url_stemcell_latest_eula=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/stemcells-ubuntu-xenial/releases" | python3 -c "import sys, json; print(json.load(sys.stdin)['releases'][0]['_links']['eula_acceptance']['href'])")
curl -s -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer ${pivtoken}" -X POST ${url_stemcell_latest_eula}
# download
url_stemcell_latest_download=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_stemcell_latest_pf}" | python3 -c "import sys, json; dict = json.load(sys.stdin)['product_files']; print( [obj for obj in dict if(obj['file_type'] == 'Software')] [0]['_links']['download']['href'])")
url_stemcell_latest_filename=$(curl -s -H "Authorization: Bearer ${pivtoken}" $(echo $url_stemcell_latest_download | sed 's@/download@@') | python3 -c "import sys, json; print(json.load(sys.stdin)['product_file']['aws_object_key'])" | sed 's@.*/@@')

### n-1
url_stemcell_n1_pf=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/stemcells-ubuntu-xenial/releases" | python3 -c "import sys, json; print(json.load(sys.stdin)['releases'][1]['_links']['product_files']['href'])")
# accept eula
url_stemcell_n1_eula=$(curl -s -H "Authorization: Bearer ${pivtoken}" "https://network.pivotal.io/api/v2/products/stemcells-ubuntu-xenial/releases" | python3 -c "import sys, json; print(json.load(sys.stdin)['releases'][1]['_links']['eula_acceptance']['href'])")
curl -s -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer ${pivtoken}" -X POST ${url_stemcell_n1_eula}
# download
url_stemcell_n1_download=$(curl -s -H "Authorization: Bearer ${pivtoken}" "${url_stemcell_n1_pf}" | python3 -c "import sys, json; dict = json.load(sys.stdin)['product_files']; print( [obj for obj in dict if(obj['file_type'] == 'Software')] [0]['_links']['download']['href'])")
url_stemcell_n1_filename=$(curl -s -H "Authorization: Bearer ${pivtoken}" $(echo $url_stemcell_n1_download | sed 's@/download@@') | python3 -c "import sys, json; print(json.load(sys.stdin)['product_file']['aws_object_key'])" | sed 's@.*/@@')

# unset
unset url_stemcell_latest_pf
unset url_stemcell_latest_eula
unset url_stemcell_n1_pf
unset url_stemcell_n1_eula

# ==================================================================
# refresh token
pivtoken=$(curl -sX POST https://network.pivotal.io/api/v2/authentication/access_tokens -d "{\"refresh_token\":\"${PIVNET_REFRESH_TOKEN}\"}" | python3 -c "import sys, json; print(json.load(sys.stdin)['access_token'])")
# launch the downloads
wget -O "${SRCFILES}/${url_om_filename}" --header="Authorization: Bearer ${pivtoken}" ${url_om_download}
echo "ova_opsman=${SRCFILES}/${url_om_filename}" >> ${VAR_FILE_INFO}
wget -O "${SRCFILES}/${url_pks_filename}" --header="Authorization: Bearer ${pivtoken}" ${url_pks_download}
echo "ova_pks=${SRCFILES}/${url_pks_filename}" >> ${VAR_FILE_INFO}
wget -O "${SRCFILES}/${url_harbor_filename}" --header="Authorization: Bearer ${pivtoken}" ${url_harbor_download}
echo "ova_harbor=${SRCFILES}/${url_harbor_filename}" >> ${VAR_FILE_INFO}
# wget -O "${SRCFILES}/${url_pfs_filename}" --header="Authorization: Bearer ${pivtoken}" ${url_pfs_download}
# echo "ova_pfs=${SRCFILES}/${url_pfs_filename}" >> ${VAR_FILE_INFO}
# stemcells download
wget -O "${SRCFILES}/${url_stemcell_latest_filename}" --header="Authorization: Bearer ${pivtoken}" ${url_stemcell_latest_download}
echo "stemcell_latest=${SRCFILES}/${url_stemcell_latest_filename}" >> ${VAR_FILE_INFO}
wget -O "${SRCFILES}/${url_stemcell_n1_filename}" --header="Authorization: Bearer ${pivtoken}" ${url_stemcell_n1_download}
echo "stemcell_n1=${SRCFILES}/${url_stemcell_n1_filename}" >> ${VAR_FILE_INFO}
# remove token
unset pivtoken
# clear vars
unset url_om_download
unset url_om_filename
unset url_pks_download
unset url_pks_filename
unset url_harbor_download
unset url_harbor_filename
# unset url_pfs_download
# unset url_pfs_filename
unset url_stemcell_latest_download
unset url_stemcell_latest_filename
unset url_stemcell_n1_download
unset url_stemcell_n1_filename

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
