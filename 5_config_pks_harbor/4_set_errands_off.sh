#!/bin/bash

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    echo "Cannot continue - file '${VAR_FILE_INFO}' not found"
    exit 1
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# get the token
uaac target https://${OPSMAN_HOST}/uaa --skip-ssl-validation
uaac token owner get opsman ${OPSMAN_ADMIN} -p ${OPSMAN_PASSWORD} -s ""
uaac_token=$(uaac context | grep access_token | sed 's/[[:space:]]*access_token: //')

# get (from file with var cache)
pks_guid=$(awk -F "=" '/pks_guid/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')

curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/errands" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
            \"errands\": [ \
                { \
                    \"name\": \"pks-nsx-t-precheck\", \
                    \"post_deploy\": false \
                }, \
                { \
                    \"name\": \"smoke-tests\", \
                    \"post_deploy\": false \
                }, \
                { \
                    \"name\": \"upgrade-all-service-instances\", \
                    \"post_deploy\": false \
                }, \
                { \
                    \"name\": \"wavefront-alert-creation\", \
                    \"post_deploy\": false \
                }, \
                { \
                    \"name\": \"delete-all-clusters\", \
                    \"pre_delete\": true \
                }, \
                { \
                    \"name\": \"wavefront-alert-deletion\", \
                    \"pre_delete\": false \
                } \
            ] \
        }"

# clear
unset uaac_token
unset pks_guid

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"