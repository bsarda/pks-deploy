#!/bin/bash

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    echo "Cannot continue - file '${VAR_FILE_INFO}' not found"
    exit 1
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# get the token
uaac target https://${OPSMAN_HOST}/uaa --skip-ssl-validation
uaac token owner get opsman ${OPSMAN_ADMIN} -p ${OPSMAN_PASSWORD} -s ""
uaac_token=$(uaac context | grep access_token | sed 's/[[:space:]]*access_token: //')

# get the guid
harbor_guid=$(awk -F "=" '/harbor_guid/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
# get the (huge!) properties bag
# curl -k "https://${OPSMAN_HOST}/api/v0/staged/products/${harbor_guid}/properties" -H "Authorization: Bearer ${uaac_token}"

# ================ network to az map ================
# set the network to az map
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${harbor_guid}/networks_and_azs" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
\"networks_and_azs\": { \
    \"singleton_availability_zone\": { \
      \"name\": \"${PKS_AZ_SINGLETON}\" \
    }, \
    \"other_availability_zones\": [ \
      { \
        \"name\": \"${PKS_AZ_OTHERJOBS}\" \
      } \
    ], \
    \"network\": { \
      \"name\": \"$(echo ${VCSA_PKS_NETWORK} | tr -d '\n' | tr -c '[[:alnum:]]' '-')\" \
    }, \
    \"service_network\": { \
      \"name\": \"${BOSH_SERVICE_NETWORK}\" \
    } \
  } \
}"

# =================== general part ==================
# Set general part
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${harbor_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.hostname\": { \
              \"value\": \"${HARBOR_HOST}\" \
            }, \
            \".properties.customize_container_network\": { \
              \"value\": \"default\" \
            } \
          } \
        }"

# ================= certificate part ================
# Set certificate part
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${harbor_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.server_cert_key\": { \
              \"value\": {\"cert_pem\": \"$(cat ${HARBOR_CERT_FILE} |  sed ':a;N;$!ba;s/\n/\\n/g')\", \"private_key_pem\": \"$(cat ${HARBOR_CERT_KEY} |  sed ':a;N;$!ba;s/\n/\\n/g')\"} \
            }, \
            \".properties.server_cert_ca\": { \
              \"value\": \"$(cat ${HARBOR_CERT_FILE} |  sed ':a;N;$!ba;s/\n/\\n/g')\" \
            } \
          } \
        }"

# ================== password part ==================
# Set password part
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${harbor_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.admin_password\": { \
              \"value\": {\"secret\": \"${HARBOR_PASSWORD}\" } \
            } \
          } \
        }"

# do not change anything else.
# for the moment, other sections will keep the default values:
# Authentication - Container Registry Storage - Clair Settings - Notary Settings - Resource Config - Errands

# clear
unset uaac_token
unset harbor_guid

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
