#!/bin/bash

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    echo "Cannot continue - file '${VAR_FILE_INFO}' not found"
    exit 1
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# get the token
uaac target https://${OPSMAN_HOST}/uaa --skip-ssl-validation
uaac token owner get opsman ${OPSMAN_ADMIN} -p ${OPSMAN_PASSWORD} -s ""
uaac_token=$(uaac context | grep access_token | sed 's/[[:space:]]*access_token: //')

# get (from file with var cache)
uploaded_harbor_name=$(awk -F "=" '/harbor_name/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
uploaded_harbor_ver=$(awk -F "=" '/harbor_ver/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')

# add to staged products
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products" -X POST -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{\"name\": \"${uploaded_harbor_name}\", \"product_version\": \"${uploaded_harbor_ver}\"}"

# get the guid
uploaded_harbor_guid=$(curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products" -H "Authorization: Bearer ${uaac_token}" | python3 -c '
import sys, json, re;
json_object=json.load(sys.stdin);
regexp = re.compile(r"harbor")
for json_sub in json_object:
  if regexp.search(json_sub["type"]):
    print(json_sub["guid"])')

# add to file
echo "harbor_guid=${uploaded_harbor_guid}" >> ${VAR_FILE_INFO}

# clear
unset uaac_token
unset uploaded_harbor_name
unset uploaded_harbor_ver
unset uploaded_harbor_guid

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
