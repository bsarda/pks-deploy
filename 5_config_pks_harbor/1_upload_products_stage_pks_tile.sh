#!/bin/bash

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    echo "Cannot continue - file '${VAR_FILE_INFO}' not found"
    exit 1
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# get the token
uaac target https://${OPSMAN_HOST}/uaa --skip-ssl-validation
uaac token owner get opsman ${OPSMAN_ADMIN} -p ${OPSMAN_PASSWORD} -s ""
uaac_token=$(uaac context | grep access_token | sed 's/[[:space:]]*access_token: //')

# get paths (from file with var cache)
ova_pks=$(awk -F "=" '/ova_pks/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
ova_harbor=$(awk -F "=" '/ova_harbor/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
ova_pfs=$(awk -F "=" '/ova_pfs/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
# and for stemcells too
stemcell_latest=$(awk -F "=" '/stemcell_latest/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
stemcell_n1=$(awk -F "=" '/stemcell_n1/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')

# upload products
echo "Uploading PKS tile"
om -t https://${OPSMAN_HOST} -k -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" upload-product --product ${ova_pks}
sleep 5

echo "Uploading Harbor tile"
om -t https://${OPSMAN_HOST} -k -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" upload-product --product ${ova_harbor}
sleep 5

#echo "Uploading PFS tile"
#om -t https://${OPSMAN_HOST} -k -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" upload-product --product ${ova_pfs}
#sleep 5

echo "Uploading Stemcell(s)"
om -t https://${OPSMAN_HOST} -k -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" upload-stemcell --stemcell ${stemcell_latest}
om -t https://${OPSMAN_HOST} -k -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" upload-stemcell --stemcell ${stemcell_n1}

# get available (after upload)
available_products=$(curl -sk "https://${OPSMAN_HOST}/api/v0/available_products" -H "Authorization: Bearer ${uaac_token}")

# consider that name for pks is pivotal-container
uploaded_pks_name_ver=$(echo ${available_products} | python3 -c '
import sys, json, re;
json_object=json.load(sys.stdin);
regexp = re.compile(r"piv.*cont")
for json_sub in json_object:
  if regexp.search(json_sub["name"]):
    print(json_sub["name"] +"/"+ json_sub["product_version"] )')
uploaded_harbor_name_ver=$(echo ${available_products} | python3 -c '
import sys, json, re;
json_object=json.load(sys.stdin);
regexp = re.compile(r"harbor")
for json_sub in json_object:
  if regexp.search(json_sub["name"]):
    print(json_sub["name"] +"/"+ json_sub["product_version"] )')
# split names and versions
uploaded_pks_name=$(echo ${uploaded_pks_name_ver} | awk -F"/" '{print $1}')
uploaded_pks_ver=$(echo ${uploaded_pks_name_ver} | awk -F"/" '{print $2}')
uploaded_harbor_name=$(echo ${uploaded_harbor_name_ver} | awk -F"/" '{print $1}')
uploaded_harbor_ver=$(echo ${uploaded_harbor_name_ver} | awk -F"/" '{print $2}')

# add to staged products
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products" -X POST -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{\"name\": \"${uploaded_pks_name}\", \"product_version\": \"${uploaded_pks_ver}\"}"

# get the guid
uploaded_pks_guid=$(curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products" -H "Authorization: Bearer ${uaac_token}" | python3 -c '
import sys, json, re;
json_object=json.load(sys.stdin);
regexp = re.compile(r"piv.*cont")
for json_sub in json_object:
  if regexp.search(json_sub["type"]):
    print(json_sub["guid"])')

# add to file
echo "pks_name=${uploaded_pks_name}" >> ${VAR_FILE_INFO}
echo "pks_ver=${uploaded_pks_ver}" >> ${VAR_FILE_INFO}
echo "pks_guid=${uploaded_pks_guid}" >> ${VAR_FILE_INFO}
echo "harbor_name=${uploaded_harbor_name}" >> ${VAR_FILE_INFO}
echo "harbor_ver=${uploaded_harbor_ver}" >> ${VAR_FILE_INFO}

# clear
unset uaac_token
unset ova_pks
unset ova_pfs
unset stemcell_latest
unset stemcell_n1
unset available_products
unset uploaded_pks_name_ver
unset uploaded_harbor_name_ver
unset uploaded_pks_name
unset uploaded_pks_ver
unset uploaded_harbor_name
unset uploaded_harbor_ver
unset uploaded_pks_guid

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
