#!/bin/bash

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    echo "Cannot continue - file '${VAR_FILE_INFO}' not found"
    exit 1
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# get the token
uaac target https://${OPSMAN_HOST}/uaa --skip-ssl-validation
uaac token owner get opsman ${OPSMAN_ADMIN} -p ${OPSMAN_PASSWORD} -s ""
uaac_token=$(uaac context | grep access_token | sed 's/[[:space:]]*access_token: //')

# get the guid
pks_guid=$(awk -F "=" '/pks_guid/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
# get the (huge!) properties bag
# curl -k "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -H "Authorization: Bearer ${uaac_token}"

# ============ Kubernetes Cloud Provider ============
# Set Kubernetes Cloud Provider
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.cloud_provider\": { \
              \"value\": \"vSphere\" \
            }, \
            \".properties.cloud_provider.vsphere.vcenter_master_creds\": { \
              \"value\": {\"identity\": \"${BOSH_IAAS_ADMIN}\", \"password\": \"${BOSH_IAAS_PASSWORD}\"} \
            }, \
            \".properties.cloud_provider.vsphere.vcenter_ip\": { \
              \"value\": \"${BOSH_IAAS_HOST}\" \
            }, \
            \".properties.cloud_provider.vsphere.vcenter_dc\": { \
              \"value\": \"${BOSH_IAAS_DATACENTER}\" \
            }, \
            \".properties.cloud_provider.vsphere.vcenter_ds\": { \
              \"value\": \"${VCSA_PKS_DATASTORE}\" \
            }, \
            \".properties.cloud_provider.vsphere.vcenter_vms\": { \
              \"value\": \"${BOSH_IAAS_FOLDER_VM}\" \
            } \
          } \
        }"

# =================== pks api part ==================
# configure hostname and cert file from previously generated
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".pivotal-container-service.pks_tls\": { \
              \"value\": {\"cert_pem\": \"$(cat ${PKS_CERT_FILE} |  sed ':a;N;$!ba;s/\n/\\n/g')\", \"private_key_pem\": \"$(cat ${PKS_CERT_KEY} |  sed ':a;N;$!ba;s/\n/\\n/g')\"} \
            }, \
            \".properties.pks_api_hostname\": { \
              \"value\": \"${PKS_HOST}\" \
            }, \
            \".properties.worker_max_in_flight\": { \
              \"value\": \"1\" \
            } \
          } \
        }"

# ================ network to az map ================
# set the network to az map
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/networks_and_azs" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
\"networks_and_azs\": { \
    \"singleton_availability_zone\": { \
      \"name\": \"${PKS_AZ_SINGLETON}\" \
    }, \
    \"other_availability_zones\": [ \
      { \
        \"name\": \"${PKS_AZ_OTHERJOBS}\" \
      } \
    ], \
    \"network\": { \
      \"name\": \"$(echo ${VCSA_PKS_NETWORK} | tr -d '\n' | tr -c '[[:alnum:]]' '-')\" \
    }, \
    \"service_network\": { \
      \"name\": \"$(echo ${VCSA_PKS_NETWORK} | tr -d '\n' | tr -c '[[:alnum:]]' '-')\" \
    } \
  } \
}"
# OLD, with service network:
# curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/networks_and_azs" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
# \"networks_and_azs\": { \
#     \"singleton_availability_zone\": { \
#       \"name\": \"${PKS_AZ_SINGLETON}\" \
#     }, \
#     \"other_availability_zones\": [ \
#       { \
#         \"name\": \"${PKS_AZ_OTHERJOBS}\" \
#       } \
#     ], \
#     \"network\": { \
#       \"name\": \"$(echo ${VCSA_PKS_NETWORK} | tr -d '\n' | tr -c '[[:alnum:]]' '-')\" \
#     }, \
#     \"service_network\": { \
#       \"name\": \"${BOSH_SERVICE_NETWORK}\" \
#     } \
#   } \
# }"

# =================== Syslog part ===================
if [[ -n ${VRLI_HOST+x} ]]
then
  echo "Configure to use vRLI"
  # set syslog for vrli
  curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.syslog_selector\": { \
              \"value\": \"disabled\" \
            }, \
            \".properties.pks-vrli\": { \
              \"value\": \"enabled\" \
            }, \
            \".properties.pks-vrli.enabled.host\": { \
              \"value\": \"${VRLI_HOST}\" \
            }, \
            \".properties.pks-vrli.enabled.use_ssl\": { \
              \"value\": \"${VRLI_USE_SSL}\" \
            }, \
            \".properties.pks-vrli.enabled.skip_cert_verify\": { \
              \"value\": \"1\" \
            }, \
            \".properties.pks-vrli.enabled.ca_cert\": { \
              \"value\": null \
            }, \
            \".properties.sink_resources_selector\": { \
              \"value\": \"enabled\" \
            } \
          } \
        }"
            # was working for 1.2:
            # \".properties.sink_resources\": { \
            #   \"value\": \"1\" \
            # } \
  #            \".properties.pks-vrli.enabled.rate_limit_msec\": { \
  #            \"value\": \"0\" \
  #          }, \
elif [[ -n ${SYSLOGSERVER+x} ]]
then
  echo "Configure to use Syslog"
  # set syslog for syslog
  curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.syslog_selector\": { \
              \"value\": \"enabled\" \
            }, \
            \".properties.syslog_selector.enabled.address\": { \
              \"value\": \"${SYSLOGSERVER}\" \
            }, \
            \".properties.syslog_selector.enabled.transport_protocol\": { \
              \"value\": \"udp\" \
            }, \
            \".properties.syslog_selector.enabled.port\": { \
              \"value\": \"514\" \
            }, \
            \".properties.syslog_selector.enabled.tls_enabled\": { \
              \"value\": \"0\" \
            }, \
            \".properties.pks-vrli\": { \
              \"value\": \"disabled\" \
            }, \
            \".properties.sink_resources_selector\": { \
              \"value\": \"enabled\" \
            } \
          } \
        }"
            # was working for 1.2:
            # \".properties.sink_resources\": { \
            #   \"value\": \"1\" \
            # } \
else
  echo "No logging at all"
  curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.syslog_selector\": { \
              \"value\": \"disabled\" \
            }, \
            \".properties.pks-vrli\": { \
              \"value\": \"disabled\" \
            }
            \".properties.sink_resources_selector\": { \
              \"value\": \"disabled\" \
            } \
          } \
        }"
            # was working for 1.2:
            # \".properties.sink_resources\": { \
            #   \"value\": \"1\" \
            # } \
fi

# =================== Network part ==================
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.network_selector\": { \
              \"value\": \"nsx\" \
            }, \
            \".properties.network_selector.nsx.nsx-t-host\": { \
              \"value\": \"${NSX_HOST}\" \
            }, \
            \".properties.network_selector.nsx.nsx-t-superuser-certificate\": { \
              \"value\": {\"cert_pem\": \"$(cat ${NSX_SUPERUSER_CERT_FILE} |  sed ':a;N;$!ba;s/\n/\\n/g')\", \"private_key_pem\": \"$(cat ${NSX_SUPERUSER_KEY_FILE} |  sed ':a;N;$!ba;s/\n/\\n/g')\"} \
            }, \
            \".properties.network_selector.nsx.nsx-t-ca-cert\": { \
              \"value\": \"$(cat ${NSX_CERT_FILE} | sed ':a;N;$!ba;s/\n/\\r\\n/g')\" \
            }, \
            \".properties.network_selector.nsx.nsx-t-insecure\": { \
              \"value\": \"1\" \
            }, \
            \".properties.network_selector.nsx.nat_mode\": { \
              \"value\": \"0\" \
            }, \
            \".properties.network_selector.nsx.ip-block-id\": { \
              \"value\": \"$(awk -F "=" '/pods_ip_block_id/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')\" \
            }, \
            \".properties.network_selector.nsx.nodes-ip-block-id\": { \
              \"value\": \"$(awk -F "=" '/nodes_ip_block_id/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')\" \
            }, \
            \".properties.network_selector.nsx.t0-router-id\": { \
              \"value\": \"${NSX_T0_ROUTER_ID}\" \
            }, \
            \".properties.network_selector.nsx.floating-ip-pool-ids\": { \
              \"value\": \"$(awk -F "=" '/vip_ip_pool_id/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')\" \
            }, \
            \".properties.network_selector.nsx.cloud-config-dns\": { \
              \"value\": \"${DNSSERVER}\" \
            }, \
            \".properties.network_selector.nsx.vcenter_cluster\": { \
              \"value\": \"${PKS_NSX_CLUSTER}\" \
            }, \
            \".properties.proxy_selector\": { \
              \"value\": \"Disabled\" \
            }, \
            \".properties.vm_extensions\": { \
              \"value\": [\"public_ip\"] \
            } \
          } \
        }"
# enable nat mode is the .properties.vm_extensions / public_ip input

# ===================== UAA part ====================
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.uaa_pks_cli_access_token_lifetime\": { \
              \"value\": \"600\" \
            }, \
            \".properties.uaa_pks_cli_refresh_token_lifetime\": { \
              \"value\": \"21600\" \
            }, \
            \".properties.uaa_oidc\": { \
              \"value\": \"1\" \
            }, \
            \".properties.uaa\": { \
              \"value\": \"internal\" \
            } \
          } \
        }"

# ================== Wavefront part =================
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.wavefront\": { \
              \"value\": \"disabled\" \
            } \
          } \
        }"

# ================== telemetry part =================
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.telemetry_selector\": { \
              \"value\": \"disabled\" \
            } \
          } \
        }"












































# ==================== plans part ===================
# kept in case of - method to disable the plans 2 and 3.
# disable plan 2
# curl -k "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
#           \"properties\": { \
#             \".properties.plan2_selector\": { \
#               \"value\": \"Plan Inactive\" \
#             } \
#           } \
#         }"
# disable plan 3
# curl -k "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
#           \"properties\": { \
#             \".properties.plan3_selector\": { \
#               \"value\": \"Plan Inactive\" \
#             } \
#           } \
#         }"

# create the az list
master_az_placement="["
worker_az_placement="["
for i in {1..9} ; do
  varname="BOSH_CONFIG_AZ_${i}_NAME"
  if [ -z ${!varname+x} ]; then break; fi
  master_az_placement="${master_az_placement}\"${!varname}\","
  worker_az_placement="${worker_az_placement}\"${!varname}\","
done
master_az_placement="${master_az_placement: : -1}]"
worker_az_placement="${worker_az_placement: : -1}]"

# set plan 1 to small (for poc)
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.plan1_selector\": { \
              \"value\": \"Plan Active\" \
            }, \
            \".properties.plan1_selector.active.name\":{ \
              \"value\": \"small\" \
            }, \
            \".properties.plan1_selector.active.description\":{ \
              \"value\": \"Small-sized K8s clusters, for PoC mainly\" \
            }, \
            \".properties.plan1_selector.active.master_az_placement\":{ \
              \"value\": ${master_az_placement} \
            }, \
            \".properties.plan1_selector.active.worker_az_placement\":{ \
              \"value\": ${worker_az_placement} \
            }, \
            \".properties.plan1_selector.active.master_vm_type\":{ \
              \"value\": \"small.disk\" \
            }, \
            \".properties.plan1_selector.active.master_persistent_disk_type\":{ \
              \"value\": \"10240\" \
            }, \
            \".properties.plan1_selector.active.worker_vm_type\":{ \
              \"value\": \"small.disk\" \
            }, \
            \".properties.plan1_selector.active.worker_persistent_disk_type\":{ \
              \"value\": \"30720\" \
            }, \
            \".properties.plan1_selector.active.master_instances\":{ \
              \"value\": \"3\" \
            }, \
            \".properties.plan1_selector.active.max_worker_instances\":{ \
              \"value\": \"12\" \
            }, \
            \".properties.plan1_selector.active.worker_instances\":{ \
              \"value\": \"3\" \
            }, \
            \".properties.plan1_selector.active.errand_vm_type\":{ \
              \"value\": \"micro\" \
            }, \
            \".properties.plan1_selector.active.addons_spec\":{ \
              \"value\": null \
            }, \
            \".properties.plan1_selector.active.allow_privileged_containers\":{ \
              \"value\": false \
            }, \
            \".properties.plan1_selector.active.disable_deny_escalating_exec\":{ \
              \"value\": false \
            } \
          } \
        }"

# set plan 2 to medium-large and sec permissive
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.plan2_selector\": { \
              \"value\": \"Plan Active\" \
            }, \
            \".properties.plan2_selector.active.name\":{ \
              \"value\": \"medsec\" \
            }, \
            \".properties.plan2_selector.active.description\":{ \
              \"value\": \"Medium-sized K8s clusters (large worker) and security permissive\" \
            }, \
            \".properties.plan2_selector.active.master_az_placement\":{ \
              \"value\": ${master_az_placement} \
            }, \
            \".properties.plan2_selector.active.worker_az_placement\":{ \
              \"value\": ${worker_az_placement} \
            }, \
            \".properties.plan2_selector.active.master_vm_type\":{ \
              \"value\": \"medium.disk\" \
            }, \
            \".properties.plan2_selector.active.master_persistent_disk_type\":{ \
              \"value\": \"10240\" \
            }, \
            \".properties.plan2_selector.active.worker_vm_type\":{ \
              \"value\": \"large\" \
            }, \
            \".properties.plan2_selector.active.worker_persistent_disk_type\":{ \
              \"value\": \"51200\" \
            }, \
            \".properties.plan2_selector.active.master_instances\":{ \
              \"value\": \"3\" \
            }, \
            \".properties.plan2_selector.active.max_worker_instances\":{ \
              \"value\": \"50\" \
            }, \
            \".properties.plan2_selector.active.worker_instances\":{ \
              \"value\": \"3\" \
            }, \
            \".properties.plan2_selector.active.errand_vm_type\":{ \
              \"value\": \"micro\" \
            }, \
            \".properties.plan2_selector.active.addons_spec\":{ \
              \"value\": null \
            }, \
            \".properties.plan2_selector.active.allow_privileged_containers\":{ \
              \"value\": true \
            }, \
            \".properties.plan2_selector.active.disable_deny_escalating_exec\":{ \
              \"value\": true \
            } \
          } \
        }"

# set plan 3 to xlarge
curl -sk "https://${OPSMAN_HOST}/api/v0/staged/products/${pks_guid}/properties" -X PUT -H "Authorization: Bearer ${uaac_token}" -H "Content-Type: application/json" -d "{ \
          \"properties\": { \
            \".properties.plan3_selector\": { \
              \"value\": \"Plan Active\" \
            }, \
            \".properties.plan3_selector.active.name\":{ \
              \"value\": \"xl\" \
            }, \
            \".properties.plan3_selector.active.description\":{ \
              \"value\": \"XLarge K8s clusters\" \
            }, \
            \".properties.plan3_selector.active.master_az_placement\":{ \
              \"value\": ${master_az_placement} \
            }, \
            \".properties.plan3_selector.active.worker_az_placement\":{ \
              \"value\": ${worker_az_placement} \
            }, \
            \".properties.plan3_selector.active.master_vm_type\":{ \
              \"value\": \"large.disk\" \
            }, \
            \".properties.plan3_selector.active.master_persistent_disk_type\":{ \
              \"value\": \"20480\" \
            }, \
            \".properties.plan3_selector.active.worker_vm_type\":{ \
              \"value\": \"xlarge.mem\" \
            }, \
            \".properties.plan3_selector.active.worker_persistent_disk_type\":{ \
              \"value\": \"51200\" \
            }, \
            \".properties.plan3_selector.active.master_instances\":{ \
              \"value\": \"3\" \
            }, \
            \".properties.plan3_selector.active.max_worker_instances\":{ \
              \"value\": \"50\" \
            }, \
            \".properties.plan3_selector.active.worker_instances\":{ \
              \"value\": \"9\" \
            }, \
            \".properties.plan3_selector.active.errand_vm_type\":{ \
              \"value\": \"micro\" \
            }, \
            \".properties.plan3_selector.active.addons_spec\":{ \
              \"value\": null \
            }, \
            \".properties.plan3_selector.active.allow_privileged_containers\":{ \
              \"value\": false \
            }, \
            \".properties.plan3_selector.active.disable_deny_escalating_exec\":{ \
              \"value\": false \
            } \
          } \
        }"

# clear
unset uaac_token
unset pks_guid
unset master_az_placement
unset worker_az_placement

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
