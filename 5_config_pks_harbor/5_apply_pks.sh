#!/bin/bash

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    echo "Cannot continue - file '${VAR_FILE_INFO}' not found"
    exit 1
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# get the token
uaac target https://${OPSMAN_HOST}/uaa --skip-ssl-validation
uaac token owner get opsman ${OPSMAN_ADMIN} -p ${OPSMAN_PASSWORD} -s ""
uaac_token=$(uaac context | grep access_token | sed 's/[[:space:]]*access_token: //')

# get (from file with var cache)
uploaded_pks_name=$(awk -F "=" '/pks_name/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
uploaded_pks_ver=$(awk -F "=" '/pks_ver/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')

# accept eula - if needed
echo "\tAccepting EULA... not critical if error"
curl -k "https://${OPSMAN_HOST}/api/v0/pivotal_network/eulas?product_name=${uploaded_pks_name}&version=${uploaded_pks_ver}&accept=true" -X PUT -H "Authorization: Bearer ${uaac_token}"

# push
om -t https://${OPSMAN_HOST} -k -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" apply-changes

# clear
unset uaac_token
unset uploaded_pks_name
unset uploaded_pks_ver

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
