#!/bin/bash

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    echo "Cannot continue - file '${VAR_FILE_INFO}' not found"
    exit 1
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# get the token
uaac target https://${OPSMAN_HOST}/uaa --skip-ssl-validation
uaac token owner get opsman ${OPSMAN_ADMIN} -p ${OPSMAN_PASSWORD} -s ""
uaac_token=$(uaac context | grep access_token | sed 's/[[:space:]]*access_token: //')

# get (from file with var cache)
harbor_name=$(awk -F "=" '/harbor_name/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
harbor_ver=$(awk -F "=" '/harbor_ver/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')

# accept eula
echo -e "\tAccepting EULA... not critical if error"
curl -k "https://${OPSMAN_HOST}/api/v0/pivotal_network/eulas?product_name=${harbor_name}&version=${harbor_ver}&accept=true" -X PUT -H "Authorization: Bearer ${uaac_token}"
# apply
om -t https://${OPSMAN_HOST} -k -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" apply-changes --skip-unchanged-products

# clear
unset uaac_token
unset harbor_name
unset harbor_ver

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
