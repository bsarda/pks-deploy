# Create sample app demo1

## Env variables

None.

## Create the App

Note that you must be logged in PKS/Kubernetes first.

```sh
kubectl apply -f demo1.yaml
```

## Check the app

get the app status and ports

```sh
kubectl -n demo1 get all
```

You can open the page from URL listed in the Service LoadBalancer IP.

## Clean it

```sh
kubectl delete pods --all --namespace demo1
kubectl delete service --all --namespace demo1
kubectl delete deployments --all --namespace demo1
kubectl delete namespace demo1
```
