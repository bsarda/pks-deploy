# Create sample app yelb

## Env variables

None.

## Launch pods and test, simple NodePort

Get the nodeport deployment

```sh
wget https://github.com/lamw/vmware-pks-app-demo/raw/master/yelb.yaml
kubectl create namespace yelb
kubectl apply -f yelb.yaml
watch -n 1 kubectl get pods --namespace yelb
kubectl --namespace yelb get service | grep yelb-ui
kubectl --namespace yelb describe pod yelb-ui | grep Node: | awk -F'/' '{print $2}'
```

## Launch pods and test, with LB

and now, try with the load balancer

```sh
wget https://github.com/lamw/vmware-pks-app-demo/raw/master/yelb-lb.yaml
kubectl apply -f yelb-lb.yaml
kubectl --namespace yelb get services | grep yelb-ui | awk -F' +|:|/' '{print $4":"$5}' | awk -F',' '{print $2}'
```

And open the page on the IP address showed before.

## Clean it

```sh
kubectl delete pods --all --namespace yelb
kubectl delete service --all --namespace yelb
kubectl delete deployments --all --namespace yelb
kubectl get all --namespace yelb
kubectl delete namespace yelb
```
