# Create sample app yelb

## Env variables

None.

## Create the namespace

```sh
kubectl apply -f ns.yaml
```

## Create the 'coffee' and 'tea' replication controller

```sh
kubectl apply -f coffee-rc.yaml
kubectl apply -f tea-rc.yaml
```

Results should like:  
replicationcontroller/coffee-rc created  
replicationcontroller/tea-rc created  

At this stage there should be 4 pods. Verify they are all up and running  

```sh
kubectl -n demo-ingress get pod
```

Results should like:  
NAME READY STATUS RESTARTS AGE  
coffee-rc-5jhhp 1/1 Running 0 40s  
coffee-rc-7fsf9 1/1 Running 0 40s  
tea-rc-jhd69 1/1 Running 0 32s  
tea-rc-nnldj 1/1 Running 0 32s  

## Create the 'coffee' and 'tea' service:

```sh
kubectl apply -f coffee-svc.yaml
kubectl apply -f tea-svc.yaml
```

Results should like:  
service/coffee-svc created  
service/tea-svc created  

Check:  

```sh
kubectl -n demo-ingress get svc
```

Results should like:  
NAME TYPE CLUSTER-IP EXTERNAL-IP PORT(S) AGE  
coffee-svc ClusterIP 10.100.200.48 <none> 80/TCP 16s  
kubernetes ClusterIP 10.100.200.1 <none> 443/TCP 11d  
tea-svc ClusterIP 10.100.200.148 <none> 80/TCP 10s  

## Generate certificate

If needed, you could generate certificate and key, or use the provided one.

Create a server certificate and private key  
This includes following steps:  

generate CA private key:  

```sh
openssl genrsa -aes256 -out ca.key 4096

Generating RSA private key, 4096 bit long modulus
..........................................++
...............................................................................................................................++
e is 65537 (0x10001)
Enter pass phrase for ca.key: <VMware1!>
Verifying - Enter pass phrase for ca.key: <VMware1!>
```

Generate CA certificate:

```sh
openssl req -key ca.key -new -x509 -days 365 -sha256 -extensions v3_ca -out ca.crt

Enter pass phrase for ca.key: <VMware1!>
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:FR
State or Province Name (full name) [Some-State]:IDF
Locality Name (eg, city) []:Paris
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Corp
Organizational Unit Name (eg, section) []:Lab
Common Name (e.g. server FQDN or YOUR name) []: cafe.example.com
Email Address []: lab@corp.local
```

Generate server certificate request and private key:  

```sh
openssl req -out server.csr -new -newkey rsa:2048 -nodes -keyout server.key

Generating a 2048 bit RSA private key
..................................................................+++
.........+++
writing new private key to 'server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:FR
State or Province Name (full name) [Some-State]:IDF
Locality Name (eg, city) []:Paris
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Corp
Organizational Unit Name (eg, section) []:Lab
Common Name (e.g. server FQDN or YOUR name) []: cafe.example.com
Email Address []: lab@corp.local
Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
```

Sign the CSR with CA certificate:

```sh
openssl x509 -req -days 360 -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt -sha256

Signature ok
subject=/C=FR/ST=IDF/L=Paris/O=Corp/OU=Lab
Getting CA Private Key
Enter pass phrase for ca.key: <VMware1!>
```

## Create the k8s secret

Push the secret from the cert and key:

```sh
kubectl -n demo-ingress create secret tls cafe-secret --key server.key --cert server.crt
```

Check:

```sh
kubectl -n demo-ingress describe secret cafe-secret
```

Result should like:  

Name: cafe-secret  
Namespace: default  
Labels: <none>  
Annotations: <none>  
Type: kubernetes.io/tls  
Data  
tls.crt: 1655 bytes  
tls.key: 1704 bytes  

## Create the ingress

Create the ingress resource cafe-ingress

```sh
kubectl apply -f cafe-ingress-https.yaml
```

The ingress resource should be updated by the virtual server IP:

```sh
kubectl -n demo-ingress get ingress
```

Results should like:  
NAME HOSTS ADDRESS PORTS AGE  
cafe-ingress cafe.example.com 10.40.14.96,100.64.112.35 80, 443 1m  

## Add a DNS entry

Add a dns host entry for the host in ingress resource.  
If using ubuntu, add a host entry in /etc/hosts.  
Eg: cafe.example.com <ip_address>  

```sh
vi /etc/hosts
```

Add the line (replace with your IP, the name is fixed):

```sh
10.40.14.96 cafe.example.com
```

## Check

Curl the ingress host for tea, verify we get served by tea-svc endpoints:  

```sh
curl -k https://cafe.example.com/tea --tlsv1.2
```

```html
<!DOCTYPE html>  
<html>  
<head>  
<title>Hello World</title>  
... 
```

Curl the ingress host for coffee, verify we get served by coffee-svc endpoints:

```sh
curl -k https://cafe.example.com/coffee --tlsv1.2
```

```html
<!DOCTYPE html>  
<html>  
<head>  
<title>Hello World</title>  
...  
```

## Clean it

```sh
kubectl delete pods --all --namespace demo-ingress
kubectl delete service --all --namespace demo-ingress
kubectl delete ingress --all --namespace demo-ingress
kubectl delete namespace demo-ingress
```
