# Access to dashboard

## Env variables

none.

## Get before

```sh
kubectl get all --namespace kube-system
kubectl describe svc/kubernetes-dashboard --namespace kube-system
```

## prepare the dashboard service yaml

the new dashboard needs new definition. create the yaml for the role and for the service

```shell
cat << EOF > kube-dash-role.yaml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
  labels:
    k8s-app: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: kubernetes-dashboard
  namespace: kube-system
EOF
cat << EOF > kube-dash-svc.yaml
apiVersion: v1
kind: Service
metadata:
  annotations:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kube-system
spec:
  ports:
  - name: https
    targetPort: 8443
    port: 8443
  selector:
    k8s-app: kubernetes-dashboard
  sessionAffinity: None
  type: LoadBalancer
EOF
```

## Delete and re-create

```sh
kubectl -n kube-system delete svc/kubernetes-dashboard
kubectl create -f kube-dash-role.yaml
kubectl create -f kube-dash-svc.yaml
```

## Get the ip and echo it

The ip of assigned lb is listed in the system svc, on the kubernetes-dashboard line:

```sh
kubectl -n kube-system get svc | grep kubernetes-dashboard | awk -F' +|:|/' '{print $4":"$5}' | awk -F',' '{print $2}'
```

Open a brower on the ip and port specified.  
When asked, select to use a config file, which is located on the machine where you are logged (with the pks get-credentials and kubectl config use-context).
The file is located by default on the user profile (`/home/user`), subdir `.kube/`, file is `config`

## Get the token

KUBE_TOKEN=$(kubectl -n kube-system describe secret default| awk '$1=="token:"{print $2}')
kubectl config set-credentials kubernetes-admin --token="${KUBE_TOKEN}"
