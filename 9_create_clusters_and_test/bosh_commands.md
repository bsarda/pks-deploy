# BOSH commands

## Env variables

None.

## Commands

Show BOSH managed VMs:

```sh
bosh -e pks vms
```

Show latest 10 recent tasks

```sh
bosh -e pks tasks -r10
```

Describe and follow a task

```sh
bosh -e pks task <taskId>
```

