# Create sample app demo1 with private registry

## Env variables

HARBOR_HOST=harbor.corp.local
REGISTRY_USER=bsarda
REGISTRY_PASS=VMware1!
REGISTRY_MAIL=bsarda@corp.local

## Create the namespace

```sh
kubectl apply -f demo-priv-registry-ns.yaml
```

## Create the secret

create the secret:

```sh
kubectl create secret -n demo-priv-registry docker-registry harborcred --docker-server=${HARBOR_HOST} --docker-username=${REGISTRY_USER} --docker-password=${REGISTRY_PASS} --docker-email=${REGISTRY_MAIL}
```

## Create the App

Note that you must be logged in PKS/Kubernetes first.

```sh
kubectl apply -f demo-priv-registry.yaml
```

## Check the app

get the app status and ports

```sh
kubectl -n demo-priv-registry get all
```

You can open the page from URL listed in the Service LoadBalancer IP.

## Clean it

```sh
kubectl delete pods --all --namespace demo-priv-registry
kubectl delete service --all --namespace demo-priv-registry
kubectl delete deployments --all --namespace demo-priv-registry
kubectl delete namespace demo-priv-registry
```

