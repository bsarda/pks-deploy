# kubectl usage

## Env variables

```shell
CLUSTER_NAME=bsa01
```

## Get vars from pks and set

Get the IP of PKS from pks:

```sh
MASTER_IP=$(pks cluster ${CLUSTER_NAME} | grep "Kubernetes Master IP" | cut -d ":" -f 2 | sed 's/[[:space:]]//g')
```

Add the record in the local host file (and dns ?)

```sh
echo "${MASTER_IP} ${CLUSTER_NAME}" | sudo tee --append /etc/hosts > /dev/null
```

## Get credentials

Get the credentials by using the pks command

```sh
pks get-credentials ${CLUSTER_NAME}
```

result is

```sh
Fetching credentials for cluster bsa01.
Context set for cluster bsa01.

You can now switch between clusters by using:
$kubectl config use-context <cluster-name>
```

the credentials are store in the `~/.kube/config` file.  
If you need to give control to a K8s administrators, then you could give them this file (before, remove it to clean other contexts ;) )

## Set context

Set the context of cluster

```sh
kubectl config use-context ${CLUSTER_NAME}
```

Get the clusters

```sh
kubectl config get-clusters
```

Set the cluster

```sh
kubectl config set-cluster ${CLUSTER_NAME}
```

## Commands

Get nodes and pods of kube-system

```sh
kubectl -n kube-system get pods
kubectl get nodes -o wide
```

Show services/pod/namespaces

```sh
kubectl get namespace
kubectl get pods --all-namespaces
kubectl get services --all-namespaces
```

Show ingress controller

```sh
kubectl describe ingress --all-namespaces
```

Create a namespace and deploy nginx

```sh
kubectl create namespace bsa
kubectl -n bsa create -f nginx/
```

Delete namespace

```sh
kubectl delete namespace bsa
```
