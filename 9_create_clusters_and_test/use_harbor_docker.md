# Docker harbor login

## Env variables

```shell
REGISTRY_HOST=harbor.corp.local
REGISTRY_USER=bsarda
REGISTRY_PASS=VMware1!
REGISTRY_PROJECT=private-book
```

## Create a user

Login to harbor UI, create a user, a project, assign the user to it.

## Download cert

```shell
sudo mkdir -p /etc/docker/certs.d/${REGISTRY_HOST}
sudo curl -k -L -u ${REGISTRY_USER}:${REGISTRY_PASS} https://${REGISTRY_HOST}/api/systeminfo/getcert -o /etc/docker/certs.d/${REGISTRY_HOST}/ca.crt
```

## And now login on it

```shell
sudo docker login ${REGISTRY_HOST} -u ${REGISTRY_USER} -p ${REGISTRY_PASS}
```

## Try to get and push

```shell
sudo docker pull nginx
sudo docker tag nginx:latest ${REGISTRY_HOST}/${REGISTRY_PROJECT}/nginx:latest
sudo docker push ${REGISTRY_HOST}/${REGISTRY_PROJECT}/nginx:latest
```

Login to harbor and check on the UI on the project, you should see the pushed image, and can click on scan