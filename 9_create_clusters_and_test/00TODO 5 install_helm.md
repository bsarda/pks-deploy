# Install helm

## Env variables

None.

## Service account

create the service account and the cluster role bind:

```sh
cat << EOF > helm-rbac.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
EOF
```

## Apply

```sh
kubectl create -f helm-rbac.yaml
helm init --service-account tiller
rm helm-rbac.yaml
```
