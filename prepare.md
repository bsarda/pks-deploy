Before everything:

- deploy VCSA
- deploy 4 ESXi
- configure vSAN cluster
- create Resource pools: pks-infra, k8s-az1, k8s-az2, k8s-az3
- deploy NSX-T mgr, controller, edge (large!!)
- deploy the helper vm
