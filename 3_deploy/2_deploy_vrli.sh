#!/bin/bash

# import the global variables
. ../0_env.sh

#govc import.spec ${opsman_ova_path} | python -m json.tool > opsman.json
export GOVC_INSECURE=1
export GOVC_URL="https://${VCSA_HOST}/sdk"
export GOVC_USERNAME=${VCSA_ADMIN}
export GOVC_PASSWORD=${VCSA_PASSWORD}
export GOVC_DATASTORE=${VCSA_PKS_DATASTORE}
export GOVC_NETWORK=${VRLI_NETWORK}
export GOVC_RESOURCE_POOL=${VRLI_RESOURCE_POOL}

cat << EOF > vrli.json
{
  "Deployment": "${VRLI_SIZING}",
  "DiskProvisioning": "thin",
  "IPAllocationPolicy": "fixedPolicy",
  "IPProtocol": "IPv4",
  "PropertyMapping": [
    {
      "Key": "vami.hostname.VMware_vCenter_Log_Insight",
      "Value": "${VRLI_HOST}"
    },
    {
      "Key": "vami.ip0.VMware_vCenter_Log_Insight",
      "Value": "${VRLI_IP}"
    },
    {
      "Key": "vami.netmask0.VMware_vCenter_Log_Insight",
      "Value": "${VRLI_NETMASK}"
    },
    {
      "Key": "vami.gateway.VMware_vCenter_Log_Insight",
      "Value": "${VRLI_GATEWAY}"
    },
    {
      "Key": "vami.DNS.VMware_vCenter_Log_Insight",
      "Value": "${DNSSERVER}"
    },
    {
      "Key": "vami.searchpath.VMware_vCenter_Log_Insight",
      "Value": "$(echo $VRLI_HOST | awk -F. '{$1="";OFS="." ; print $0}' | sed 's/^.//')"
    },
    {
      "Key": "vami.domain.VMware_vCenter_Log_Insight",
      "Value": "$(echo $VRLI_HOST | awk -F. '{$1="";OFS="." ; print $0}' | sed 's/^.//')"
    },
    {
      "Key": "vami.preferipv6.VMware_vCenter_Log_Insight",
      "Value": "False"
    },
    {
      "Key": "vm.rootpw",
      "Value": "${VRLI_PASSWORD}"
    },
    {
      "Key": "vm.sshkey",
      "Value": ""
    }
  ],
  "NetworkMapping": [
    {
      "Name": "Network 1",
      "Network": "${VRLI_NETWORK}"
    }
  ],
  "Annotation": "VMware vRealize Log Insight",
  "MarkAsTemplate": false,
  "PowerOn": true,
  "InjectOvfEnv": true,
  "WaitForIP": false,
  "Name": "$(echo $VRLI_HOST | awk -F. '{print $1}')"
}
EOF

govc import.ova --options=vrli.json ${vrli_ova_path}
rm vrli.json

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
