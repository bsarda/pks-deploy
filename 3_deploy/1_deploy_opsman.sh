#!/bin/bash

# import the global variables
. ../0_env.sh

opsman_ova_path=$(cat ${VAR_FILE_INFO} | grep ova_opsman | awk -F'=' '{print $2}')
opsman_shortname=$(echo $OPSMAN_HOST | awk -F. '{print $1}')

# get the spec for import - in case of change
#govc import.spec ${opsman_ova_path} | python3 -m json.tool > opsman.json

# set the govc env var
export GOVC_INSECURE=1
export GOVC_URL="https://${VCSA_HOST}/sdk"
export GOVC_USERNAME=${VCSA_ADMIN}
export GOVC_PASSWORD=${VCSA_PASSWORD}
export GOVC_DATASTORE=${VCSA_PKS_DATASTORE}
export GOVC_NETWORK=${VCSA_PKS_NETWORK}
export GOVC_RESOURCE_POOL=${VCSA_PKS_RESOURCE_POOL}

cat << EOF > opsman.json
{
  "DiskProvisioning": "thin",
  "IPAllocationPolicy": "fixedPolicy",
  "IPProtocol": "IPv4",
  "MarkAsTemplate": false,
  "PowerOn": true,
  "InjectOvfEnv": true,
  "WaitForIP": true,
  "Name": "${opsman_shortname}",
  "PropertyMapping": [
    {
      "Key": "custom_hostname",
      "Value": "${opsman_shortname}"
    },
    {
      "Key": "ip0",
      "Value": "${OPSMAN_IP_ADDRESS}"
    },
    {
      "Key": "netmask0",
      "Value": "${OPSMAN_NETMASK}"
    },
    {
      "Key": "gateway",
      "Value": "${OPSMAN_GATEWAY}"
    },
    {
      "Key": "DNS",
      "Value": "${DNSSERVER}"
    },
    {
      "Key": "ntp_servers",
      "Value": "${NTPSERVER}"
    },
    {
      "Key": "admin_password",
      "Value": "${OPSMAN_PASSWORD}"
    },
    {
      "Key": "public_ssh_key",
      "Value": ""
    }
  ],
 "NetworkMapping": [
    {
      "Name": "Network 1",
      "Network": "${VCSA_PKS_NETWORK}"
    }
  ]
}
EOF

govc import.ova --options=opsman.json ${opsman_ova_path}
rm opsman.json
unset opsman_ova_path
unset opsman_shortname

# wait until it's true
while true
  do opsman_login_rc=$(curl -Lks -o /dev/null -w "%{http_code}" "https://${OPSMAN_HOST}/login/ensure_availability")
  if [ "$opsman_login_rc" -eq 200 ]; then
    break
  fi
  echo "Waiting for OpsMan availability, current code: ${opsman_login_rc}"
  sleep 10
done

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
