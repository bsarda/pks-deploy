#!/bin/bash
echo -e "\n\n"
echo -e "\e[103m\e[34m==========================================\e[39m\e[49m"
echo -e "\e[103m\e[34mPlease be sure you included records in DNS\e[39m\e[49m"
echo -e "\n\n"

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    touch "${VAR_FILE_INFO}"
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# get cert
if [ ! -e "${OPSMAN_CERT_FILE}" ] ; then
om -t https://${OPSMAN_HOST} -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" -k curl -p /api/v0/certificate_authorities -s | python3 -c '
import sys, json;
jason=json.load(sys.stdin);
for j in jason["certificate_authorities"]:
  if j["active"] == True:
    print(j["cert_pem"])' > ${OPSMAN_CERT_FILE}
fi

# get bosh host
# try to get from env if exists
BOSH_ENVIRONMENT=$(awk -F "=" '/bosh_environment/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
if [ -z ${BOSH_ENVIRONMENT} ] ; then
    BOSH_ENVIRONMENT=$(om -t https://${OPSMAN_HOST} -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" -k bosh-env | grep BOSH_ENVIRONMENT | awk -F'=' '{print $2}')
    if [ -z ${BOSH_ENVIRONMENT} ] ; then
        echo -e "\n\n"
        echo -e "\e[41m\e[97m"'!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!'"\e[49m\e[39m"
        echo -e "\e[41m\e[97m"'Failed to retrieve BOSH_ENVIRONMENT from opsman.'"\e[49m\e[39m"
        exit 10
    fi
    # add to file
    echo "bosh_environment=${BOSH_ENVIRONMENT}" >> ${VAR_FILE_INFO}
fi

# same for password
# try to get from env if exists
BOSH_PASSWORD=$(awk -F "=" '/bosh_password/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
if [ -z ${BOSH_PASSWORD} ] ; then
    BOSH_PASSWORD=$(om -t https://${OPSMAN_HOST} -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" -k curl -p /api/v0/deployed/director/credentials/director_credentials -s | python3 -c "import sys, json; print(json.load(sys.stdin)['credential']['value']['password'])")
    if [ -z ${BOSH_PASSWORD} ] ; then
        echo -e "\n\n"
        echo -e "\e[41m\e[97m"'!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!'"\e[49m\e[39m"
        echo -e "\e[41m\e[97m"'Failed to retrieve BOSH_PASSWORD from opsman.'"\e[49m\e[39m"
        exit 20
    fi
    # add to file
    echo "bosh_password=${BOSH_PASSWORD}" >> ${VAR_FILE_INFO}
fi

# login bosh
bosh alias-env pks -e="${BOSH_ENVIRONMENT}" --ca-cert ${OPSMAN_CERT_FILE}
if [ $(echo $?) != 0 ] ; then
  echo -e "\n\n"
  echo -e "\e[41m\e[97m"'!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!'"\e[49m\e[39m"
  echo -e "\e[41m\e[97m"'Failed to alias BOSH environment.'"\e[49m\e[39m"
  exit 30
fi

echo -e "director\n${BOSH_PASSWORD}" | bosh -e pks log-in
if [ $(echo $?) != 0 ] ; then
  echo -e "\n\n"
  echo -e "\e[41m\e[97m"'!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!'"\e[49m\e[39m"
  echo -e "\e[41m\e[97m"'Failed to connect to BOSH CLI.'"\e[49m\e[39m"
  exit 40
fi

# clear
unset BOSH_ENVIRONMENT
unset BOSH_PASSWORD

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
