#!/bin/bash
echo -e "\n\n"
echo -e "\e[103m\e[34m==========================================\e[39m\e[49m"
echo -e "\e[103m\e[34mPlease be sure you included records in DNS\e[39m\e[49m"
echo -e "\n\n"

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    touch "${VAR_FILE_INFO}"
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# env get
pks_guid=$(awk -F "=" '/pks_guid/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
# get secret
ADMIN_SECRET=$(om -t https://${OPSMAN_HOST} -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" -k curl -p /api/v0/deployed/products/${pks_guid}/credentials/.properties.pks_uaa_management_admin_client -s | python3 -c "import sys, json; print(json.load(sys.stdin)['credential']['value']['secret'])"  | tr -d '""')

# create
uaac target https://${PKS_HOST}:8443 --skip-ssl-validation
uaac token client get admin -s "${ADMIN_SECRET}"
uaac user add ${PKS_USER_NAME} --emails ${PKS_USER_NAME}@${PKS_HOST} -p ${PKS_USER_PASSWORD}
uaac member add pks.clusters.admin ${PKS_USER_NAME}
uaac token delete --all
# clear
unset pks_guid
unset ADMIN_SECRET

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
