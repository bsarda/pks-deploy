#!/bin/bash
echo -e "\n\n"
echo -e "\e[103m\e[34m==========================================\e[39m\e[49m"
echo -e "\e[103m\e[34mPlease be sure you included records in DNS\e[39m\e[49m"
echo -e "\n\n"

# import the global variables
. ../0_env.sh

# login pks
pks login -a https://${PKS_HOST}:9021 -u ${PKS_USER_NAME} -p ${PKS_USER_PASSWORD} -k
if [ $(echo $?) != 0 ] ; then
  echo -e "\n\n"
  echo -e "\e[41m\e[97m"'!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!'"\e[49m\e[39m"
  echo -e "\e[41m\e[97m"'Failed to login on PKS. Please review env file.'"\e[49m\e[39m"
else
  # finished !
  echo -e "\n\n"
  echo -e "\e[92m========================================\e[39m"
  echo -e "\e[92mThis script is now finished successfully\e[39m\n"
fi
