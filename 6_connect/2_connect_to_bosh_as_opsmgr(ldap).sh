#!/bin/bash
echo -e "\n\n"
echo -e "\e[103m\e[34m==========================================\e[39m\e[49m"
echo -e "\e[103m\e[34mPlease be sure you included records in DNS\e[39m\e[49m"
echo -e "\n\n"

# import the global variables
. ../0_env.sh

# init file appender
if [ ! -e "${VAR_FILE_INFO}" ] ; then
    touch "${VAR_FILE_INFO}"
fi

if [ ! -w "${VAR_FILE_INFO}" ] ; then
    echo "cannot write to ${VAR_FILE_INFO}"
    exit 1
fi

# get cert
if [ ! -e "${OPSMAN_CERT_FILE}" ] ; then
om -t https://${OPSMAN_HOST} -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" -k curl -p /api/v0/certificate_authorities -s | python3 -c '
import sys, json;
jason=json.load(sys.stdin);
for j in jason["certificate_authorities"]:
  if j["active"] == True:
    print(j["cert_pem"])' > ${OPSMAN_CERT_FILE}
fi
BOSH_CA_CERT=$(realpath ${OPSMAN_CERT_FILE})
echo "BOSH_CA_CERT=${BOSH_CA_CERT}" >> ${VAR_FILE_INFO}

# get bosh host
# try to get from env if exists
BOSH_ENVIRONMENT=$(awk -F "=" '/BOSH_ENVIRONMENT/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
if [ -z ${BOSH_ENVIRONMENT} ] ; then
    BOSH_ENVIRONMENT=$(om -t https://${OPSMAN_HOST} -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" -k bosh-env | grep BOSH_ENVIRONMENT | awk -F'=' '{print $2}')
    if [ -z ${BOSH_ENVIRONMENT} ] ; then
        echo -e "\n\n"
        echo -e "\e[41m\e[97m"'!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!'"\e[49m\e[39m"
        echo -e "\e[41m\e[97m"'Failed to retrieve BOSH_ENVIRONMENT from opsman.'"\e[49m\e[39m"
        exit 10
    fi
    # add to file
    echo "BOSH_ENVIRONMENT=${BOSH_ENVIRONMENT}" >> ${VAR_FILE_INFO}
fi

# same for password
# try to get from env if exists
BOSH_CLIENT=$(awk -F "=" '/bosh_client/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
if [ -z ${BOSH_CLIENT} ] ; then
    BOSH_CLIENT=$(om -t https://${OPSMAN_HOST} -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" -k curl -p /api/v0/deployed/director/credentials/bosh_commandline_credentials -s | grep "credential" | sed 's/.*BOSH_CLIENT=\([a-zA-Z0-9\-]*\)/\1/' | sed 's/[[:space:]]*BOSH_.*//')
    if [ -z ${BOSH_CLIENT} ] ; then
        echo -e "\n\n"
        echo -e "\e[41m\e[97m"'!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!'"\e[49m\e[39m"
        echo -e "\e[41m\e[97m"'Failed to retrieve BOSH_CLIENT from opsman.'"\e[49m\e[39m"
        exit 20
    fi
    # add to file
    echo "BOSH_CLIENT=${BOSH_CLIENT}" >> ${VAR_FILE_INFO}
fi

BOSH_CLIENT_SECRET=$(awk -F "=" '/bosh_client_secret/ {print $2}' ${VAR_FILE_INFO} | tr -d ' ')
if [ -z ${BOSH_CLIENT_SECRET} ] ; then
    BOSH_CLIENT_SECRET=$(om -t https://${OPSMAN_HOST} -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" -k curl -p /api/v0/deployed/director/credentials/bosh_commandline_credentials -s | grep "credential" | sed 's/.*BOSH_CLIENT_SECRET=\([a-zA-Z0-9\-]*\)/\1/' | sed 's/[[:space:]]*BOSH_.*//')
    if [ -z ${BOSH_CLIENT_SECRET} ] ; then
        echo -e "\n\n"
        echo -e "\e[41m\e[97m"'!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!'"\e[49m\e[39m"
        echo -e "\e[41m\e[97m"'Failed to retrieve BOSH_CLIENT_SECRET from opsman.'"\e[49m\e[39m"
        exit 25
    fi
    # add to file
    echo "BOSH_CLIENT_SECRET=${BOSH_CLIENT_SECRET}" >> ${VAR_FILE_INFO}
fi

# export to all
export BOSH_CLIENT
export BOSH_CLIENT_SECRET
export BOSH_CA_CERT
export BOSH_ENVIRONMENT


# login bosh
bosh log-in
if [ $(echo $?) != 0 ] ; then
  echo -e "\n\n"
  echo -e "\e[41m\e[97m"'!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!'"\e[49m\e[39m"
  echo -e "\e[41m\e[97m"'Failed to connect to BOSH CLI.'"\e[49m\e[39m"
  exit 40
fi

# finished !
echo -e "\n\n"
echo -e "\e[92m========================================\e[39m"
echo -e "\e[92mThis script is now finished successfully\e[39m\n"
echo -e "\n\n"
echo -e "\e[103m\e[34m==========================================\e[39m\e[49m"
echo -e "\e[103m\e[34mFor using BOSH, please source file, code:\e[39m\e[49m"
echo -e "\e[103m\e[34msource ${VAR_FILE_INFO}\e[39m\e[49m"
echo -e "\n\n"

