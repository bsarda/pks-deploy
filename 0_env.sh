#!/bin/bash
# ------------- /!\ secure section /!\ -------------
# The critical and secure infos. Needs to be adjuster to yours.
export PIVNET_REFRESH_TOKEN="somewhat"

# ------------------- basic info -------------------
# where to download files
export SRCFILES="/mnt/data"
export VAR_FILE_INFO="../var_infos.txt"
# base dirs
export BINDIR="/usr/local/bin"

# golang
export GOROOT="/usr/local/go"
export GOPATH="$HOME/go"

# common infra
export DNSSERVER="172.16.4.5"
export NTPSERVER="172.16.4.5"
export SYSLOGSERVER="172.16.4.3" # considering port 514 udp, no ssl. if vRLI section is configured, ignoring this

# versions
export VERSIONS_DESIRED_OPSMAN="2.4"
export VERSIONS_DESIRED_PKS="1.3"
export VERSIONS_DESIRED_HARBOR="1.7"
export VERSIONS_DESIRED_PFS="1.0"

# ------------------- vrli config ------------------
export VRLI_HOST="lab4-vrli.corp.local"
export VRLI_IP="172.16.4.3"
export VRLI_USE_SSL="1" # 0 or 1, text
export VRLI_PASSWORD="VMware1!"
export VRLI_SIZING="xsmall"   # xsmall, small, medium, large
export VRLI_DATASTORE="vsanDatastore"
export VRLI_NETWORK="VM Network"
export VRLI_NETMASK="255.255.255.0"
export VRLI_GATEWAY="172.16.4.1"
export VRLI_RESOURCE_POOL=""
export VRLI_OVA_PATH="/mnt/data/VMware-vRealize-Log-Insight-4.7.0-9983377_OVF10.ova"

# ------------------- nsx config -------------------
export NSX_HOST="lab4-nsxmgr.corp.local"
export NSX_ADMIN="admin"
export NSX_PASSWORD="VMware1!"
# t0 router id for being used by PKS
export NSX_T0_ROUTER_ID="dc553ff1-7b2c-4e0e-afd4-0ddb32707ab7"
# nsx ui/api cert as replacement as the original
export NSX_CERT_COUNTRY="FR"
export NSX_CERT_STATE="IDF"
export NSX_CERT_LOCALITY="Paris"
export NSX_CERT_ORG="Corp"
export NSX_CERT_KEY="../nsx.key"
export NSX_CERT_FILE="../nsx.crt"
# nsx superuser (used by pks for automation
export NSX_SUPERUSER_NAME="pks-nsx-t-superuser"
export NSX_SUPERUSER_CERT_FILE="../pks-nsx-t-superuser.crt"
export NSX_SUPERUSER_KEY_FILE="../pks-nsx-t-superuser.key"
# misc
export NODE_ID=$(cat /proc/sys/kernel/random/uuid)
# Nodes IP block - allocation for k8s masters and workers IP addresses.
# does not need external connectivity. need to specify /23 or greater, will split in /24
export NODE_IP_BLOCK_NAME="pks-nodes"
export NODE_IP_BLOCK_DESC="PKS IP Block for NODES"
export NODE_IP_BLOCK_CIDR="172.104.0.0/18"
# Pods IP block - allocation for the deployed pods inside the k8s workers.
# might require external connectivity. need to specify /23 or greater, will split in /24
export POD_IP_BLOCK_NAME="pks-pods"
export POD_IP_BLOCK_DESC="PKS IP Block for PODS"
export POD_IP_BLOCK_CIDR="172.104.64.0/18"
# Nodes IP block - allocation for k8s masters and workers IP addresses.
export VIP_IP_POOL_NAME="pks-vips"
export VIP_IP_POOL_DESC="PKS VIP IP Pool"
export VIP_IP_POOL_ALLOCATION_START="172.104.255.10"
export VIP_IP_POOL_ALLOCATION_END="172.104.255.254"
export VIP_IP_POOL_ALLOCATION_CIDR="172.104.255.0/24"

# ------------ where to deploy settings -------------
# vcenter server - used also for BOSH setup
export VCSA_HOST="lab4-vcsa.corp.local"
export VCSA_ADMIN="administrator@vsphere.local"
export VCSA_PASSWORD="VMware1!"
# where to deploy infra (opsman,...)
export VCSA_PKS_DATASTORE="vsanDatastore"
export VCSA_PKS_NETWORK="LS_PKSInfra"
export VCSA_PKS_RESOURCE_POOL="pks-infra"

# pivotal opsmanager
export OPSMAN_HOST="lab4-opsmgr.corp.local"
export OPSMAN_IP_ADDRESS="172.16.41.5"
export OPSMAN_NETMASK="255.255.255.0"
export OPSMAN_GATEWAY="172.16.41.1"
export OPSMAN_ADMIN="admin"
export OPSMAN_PASSWORD="VMware1!"
export OPSMAN_PASSPHRASE="VMware1!"

# ------------------- bosh config -------------------
# iaas
export BOSH_IAAS_HOST="lab4-vcsa.corp.local"
export BOSH_IAAS_NAME=$(echo $BOSH_IAAS_HOST | awk -F'.' '{print $1}')   # same than host, but short name
export BOSH_IAAS_DATACENTER="LAB4"
export BOSH_IAAS_ADMIN="administrator@vsphere.local"
export BOSH_IAAS_PASSWORD="VMware1!"
export BOSH_IAAS_DATASTORE_EPHEMERAL="vsanDatastore"
export BOSH_IAAS_DATASTORE_PERSISTANT="vsanDatastore"
export BOSH_IAAS_FOLDER_VM="pks-folder"
export BOSH_IAAS_FOLDER_TEMPLATES="pks-template-folder"
export BOSH_IAAS_FOLDER_DISK="pks-disk-location"
# iaas nsx part (defaults to same than nsx admin)
export BOSH_NSX_ADMIN=${NSX_ADMIN}
export BOSH_NSX_PASSWORD=${NSX_PASSWORD}
# director uses the ntpserver from commons
# az name and cluster/resourcepool (separator is /)
export BOSH_CONFIG_AZ_1_NAME="az1"
export BOSH_CONFIG_AZ_1_CLUSTER_RESOURCEPOOL="LAB4/k8s-az1"
export BOSH_CONFIG_AZ_2_NAME="az2"
export BOSH_CONFIG_AZ_2_CLUSTER_RESOURCEPOOL="LAB4/k8s-az2"
export BOSH_CONFIG_AZ_3_NAME="az3"
export BOSH_CONFIG_AZ_3_CLUSTER_RESOURCEPOOL="LAB4/k8s-az3"
  # and the pks infra - where is located pks, harbor, opsman...
export BOSH_CONFIG_AZ_I_NAME="pks-infra"
export BOSH_CONFIG_AZ_I_CLUSTER_RESOURCEPOOL="LAB4/pks-infra"	
# dns uses the dnsserver from commons
# syslog uses the syslogserver from commons
# network for infra and for "service" (k8s clusters)

# create the az list (excl. infra az)
az_names="["
for i in {1..9} ; do
varname="BOSH_CONFIG_AZ_${i}_NAME"
if [ -z ${!varname+x} ]; then break; fi
az_names="${az_names}\"${!varname}\","
done
# add the az of mgmt
az_names="${az_names}\"${BOSH_CONFIG_AZ_I_NAME}\","
# remove trailing , and add ]
az_names="${az_names: : -1}]"

export BOSH_NETWORKS="{\
    \"name\": \"$(echo ${VCSA_PKS_NETWORK} | tr -d '\n' | tr -c '[[:alnum:]]' '-')\",
    \"subnets\": [{ \
      \"iaas_identifier\": \"${VCSA_PKS_NETWORK}\", \
      \"cidr\": \"172.16.41.0/24\", \
      \"reserved_ip_ranges\": \"172.16.41.1-172.16.41.10\", \
      \"dns\": \"172.16.4.5\", \
      \"gateway\": \"172.16.41.1\",\
      \"availability_zone_names\": ${az_names} \
    }] \
  }"

# OLD way to handle it - 1.1 version
# export BOSH_NETWORKS_I="{\
#     \"name\": \"$(echo ${VCSA_PKS_NETWORK} | tr -d '\n' | tr -c '[[:alnum:]]' '-')\",
#     \"subnets\": [{ \
#       \"iaas_identifier\": \"${VCSA_PKS_NETWORK}\", \
#       \"cidr\": \"172.16.41.0/24\", \
#       \"reserved_ip_ranges\": \"172.16.41.1-172.16.41.10\", \
#       \"dns\": \"172.16.4.5\", \
#       \"gateway\": \"172.16.41.1\",\
#       \"availability_zone_names\": [\"${BOSH_CONFIG_AZ_I_NAME}\"] \
#     }] \
#   }"
# export BOSH_SERVICE_NETWORK="PKS-K8s-clusters"
# use fictive cidr since it's no longer used in pks1.2
# export BOSH_NETWORKS_AZ="{ \
#     \"name\": \"${BOSH_SERVICE_NETWORK}\", \
#     \"subnets\": [{ \
#       \"iaas_identifier\": \"${VCSA_PKS_NETWORK}\", \
#       \"cidr\": \"192.168.100.0/24\", \
#       \"reserved_ip_ranges\": \"192.168.100.1-192.168.100.9\", \
#       \"dns\": \"${DNSSERVER}\", \
#       \"gateway\": \"192.168.100.1\", \
#       \"availability_zone_names\": ${az_names} \
#     }] \
#   }"

export BOSH_NET2AZ="$(echo ${VCSA_PKS_NETWORK} | tr -d '\n' | tr -c '[[:alnum:]]' '-')/${BOSH_CONFIG_AZ_I_NAME}"   # network to az map - separator is /

# ------------------- pks config -------------------
export PKS_HOST="pksapi.corp.local"
# pks api cert to create
export PKS_CERT_COUNTRY="FR"
export PKS_CERT_STATE="IDF"
export PKS_CERT_LOCALITY="Paris"
export PKS_CERT_ORG="Corp"
export PKS_CERT_KEY="../pksapi.key"
export PKS_CERT_FILE="../pksapi.crt"

# for network to az
export PKS_AZ_SINGLETON=${BOSH_CONFIG_AZ_I_NAME}
export PKS_AZ_OTHERJOBS=${BOSH_CONFIG_AZ_I_NAME}
# cluster for network nsx part
export PKS_NSX_CLUSTER="LAB4"

# ------------------ harbor config -----------------
export HARBOR_HOST="harbor.corp.local"
# harbor api cert to create
export HARBOR_CERT_COUNTRY="FR"
export HARBOR_CERT_STATE="IDF"
export HARBOR_CERT_LOCALITY="Paris"
export HARBOR_CERT_ORG="Corp"
export HARBOR_CERT_KEY="../harborapi.key"
export HARBOR_CERT_FILE="../harborapi.crt"
# password
export HARBOR_PASSWORD="VMware1!"

# ==================================================
# ------------- post config and connect ------------
export OPSMAN_CERT_FILE="../opsman-ca.crt"
# pks user to be created
export PKS_USER_NAME="bsarda"
export PKS_USER_PASSWORD="VMware1!"

